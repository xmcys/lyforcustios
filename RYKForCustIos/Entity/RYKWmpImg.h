//
//  RYKWmpImg.h
//  RYKForCustIos
//
//  Created by hsit on 14-11-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKWmpImg : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *menuCode;
@property (nonatomic, strong) NSString *menuName;
@property (nonatomic, strong) NSString *picUrl;
@property (nonatomic, strong) NSString *flag;

- (void)parseData:(NSData *)data complete:(void(^)(RYKWmpImg *item))block;

@end
