//
//  CustWebService.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RYKCustInfo.h"

#define WEBSERVICE_HOST @"http://www.fjlyyc.com"
//#define WEBSERVICE_HOST @"http://happycorp.chinacloudapp.cn"
#define WEBSERVICE_FILE_HOST @"http://xm.fj-tobacco.com:18000/assets"
#define WEBSERVICE_ITEM_IMAGE_URL @"http://www.fjlyyc.com:18000/assets/"

#define WEBSERVICE_IMAGE_HOST @"http://www.fjlyyc.com"

#define XMPP_SERVICE_HOST @"220.162.158.51"
//#define XMPP_SERVICE_HOST @"elephant.chinacloudapp.cn"
#define XMPP_SERVICE_NAME @"webserver"
//#define XMPP_SERVICE_NAME @"127.0.0.1"
#define XMPP_SERVICE_PORT 5222

static NSString * APP_TYPE = @"1";  // 1 正版 2 越狱

@interface RYKWebService : NSObject

+ (void)setUser:(RYKCustInfo *)info;
+ (RYKCustInfo *)sharedUser;

// 文件具体路径
+ (NSString *)fileFullUrl:(NSString *)path;
// 物品图片具体路径
+ (NSString *)itemImageFullUrl:(NSString *)path;
// 即时聊天对象文件夹
+ (NSString *)msgTargetDir:(NSString *)msgTarget;
// 模块使用记录
+ (NSString *)cmappLogInfoUrl:(NSString *)userId appType:(NSString *)appType version:(NSString *)version modCode:(NSString *)modCode operateId:(NSString *)operateId;
// 版本检查
+ (NSString *)cmappVersionCheckUrl;
// 客户登陆
+ (NSString *)custSignInUrl:(NSString *)userCode pwd:(NSString *)pwd;
// 修改密码
+ (NSString *)savePasswordUrl;
// 许可证号、今日关注者、今日完成订单
+ (NSString *)getUserMessage;
// 待处理订单
+ (NSString *)getOrderStatusUrl;
// 查询公告信息和查询公告信息
+ (NSString *)queryUsMsgList:(NSString *)msg;
// 获取目标公告信息
+ (NSString *)getUsMsgListById:(NSString *)msgId publishman:(NSString *)publishman;
// 新增公告
+ (NSString *)saveUsMsgList;
// 删除公告
+ (NSString *)deleteUsMsgList:(NSString *)msgId;
// 更新公告信息
+ (NSString *)updateUsMsgList;
// 商品类型url
+ (NSString *)getGoodsTypeUrl;
// 商品筛选url
+ (NSString *)getGoodsListUrl;
// 商品上下架url
+ (NSString *)changeGoodsStateUrl;
// 商品规格url
+ (NSString *)getGoodsUnitUrl:(NSString *)typeId;
// 新增商品url
+ (NSString *)addNewGoodsUrl;
// 修改商品url
+ (NSString *)modifyGoodsUrl;
// 保存商品促销信息url
+ (NSString *)saveGoodsOnSaleInfoUrl;
// 查询商品信息in非烟库
+ (NSString *)queryGoodsInfoUrl:(NSString *)barCode;
// 获取订单信息
+ (NSString *)getOrderFlow:(NSString *)flowCode withFlowStatus:(NSString *)flowStatus;
// 获取指定订单详情
+ (NSString *)getOrderDetail:(NSString *)orderId;
// 订单确认合并接口url
+ (NSString *)getOrderConfirmUrl:(NSString *)orderId withFlowCode:(NSString *)flowCode withTime:(NSString *)time;
// 获取订单追踪url
+ (NSString *)getOrderTrace:(NSString *)orderId;
// 店铺信息数据读取
+ (NSString *)getReadStoreInfo:(NSString *)userId;
// 店铺信息数据保存
+ (NSString *)getSaveStoreInfo;
// 送货服务内容获取
+ (NSString *)getDeliveryContent;
// 送货服务内容保存
+ (NSString *)getSaveDeliveryContent;
// 获取不同类型的留言内容
+ (NSString *)getMoreMessage:(NSString *)opinionFlag conFlag:(NSString *)conFlag dateFlag:(NSString *)dateFlag;
// 删除留言
+ (NSString *)getDeleteMessage;
// 留言回复的保存
+  (NSString *)getSaveMessage;
// 搜索目标消费者资料
+ (NSString *)getCustInfo;
// 获取消费者信息
+ (NSString *)getConsumerInfo;
// 显示【消费者资料】界面的顾客的订单汇总信息
+ (NSString *)getOrderId:(NSString *)userId;
// 显示消费者ORDER_ID,详情
+ (NSString *)getOrderIdDetail:(NSString *)userId;
// 显示消费记录列表，按照订单日期倒序排列和显示具体的订单详情
+ (NSString *)getListRecords;
// 销售信息url
+ (NSString *)getSalesOrderListUrl;
// 条码搜出销售信息url
+ (NSString *)getBarCodeSalesOrderListUrl;
// 上传图片url
+ (NSString *)postImageUrl;
// 更新商品单价
+ (NSString *)updateGoodsPrice:(NSString *)orderId brandId:(NSString *)brandId price:(NSString *)price;
// 更新订单总金额
+ (NSString *)upOrderPrice:(NSString *)orderId;
// 更新手机设备号 
+ (NSString *)upDeviceToken:(NSString *)token;
// 聊天上传图片url
+ (NSString *)chatPostImageUrl;
// 获取微名片的下拉框数据
+ (NSString *)getDictData;
// 获取微名片的图片菜单
+ (NSString *)getDisplayImages:(NSString *)userId menuCode:(NSString *)menuCode;
// 修改微名片的图片
+ (NSString *)getSavePic;
// 恢复微名片默认图片
+ (NSString *)getRestoreDefault;
// 商品分享链接
+ (NSString *)getLinkAddress:(NSString *)userId;
// 获取商品库统计数据
+ (NSString *)getBrandList:(NSString *)userId;
// 获取类别列表
+ (NSString *)getBrandTypeList;
// 类别上移
+ (NSString *)typeMoveUpUrl;
// 类别下移
+ (NSString *)typeMoveDownUrl;
// 类别置顶
+ (NSString *)typeMoveToTopUrl;
// 类别排序(上移，下移，置顶)通用post参数
+ (NSString *)typeSortParams:(NSString *)type serialNo:(NSString *)serialNo;
// 获取商品排序数据
+ (NSString *)getGoodsSortDisplay:(NSString *)isTabacco;
+ (NSString *)goodsMoveUpUrl;
+ (NSString *)goodsMoveDownUrl;
+ (NSString *)goodsMoveToTopUrl;
// 商品排序(上移，下移，置顶)通用post参数
+ (NSString *)goodsSortParams:(NSString *)brandId serialNo:(NSString *)serialNo isTabacco:(NSString *)isTabacco;

@end
