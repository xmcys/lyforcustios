//
//  RYKDeviceToken.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-8-1.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKDeviceToken : NSObject

@property (nonatomic, strong) NSString *deviceToken;

+ (RYKDeviceToken *)sharedManager;

@end
