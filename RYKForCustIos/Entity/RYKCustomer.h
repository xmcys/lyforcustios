//
//  RYKCustomer.h
//  RYKForCustIos
//
//  Created by hsit on 14-7-1.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKCustomer : NSObject<NSXMLParserDelegate>
@property (nonatomic, strong) NSString * userId;               //序列号
@property (nonatomic, strong) NSString * userName;            //客户姓名
@property (nonatomic, strong) NSString * contactName;         // 消费者姓名
@property (nonatomic, strong) NSString * mobilphone;         // 消费者电话
@property (nonatomic, strong) NSString * sex;                 // 性别
@property (nonatomic, strong) NSString * email;               // 邮箱
@property (nonatomic, strong) NSString * birthday;            // 生日
@property (nonatomic, strong) NSString * regisTime;           // 注册日期
@property (nonatomic, strong) NSString * userAddr;            // 用户地址
@property (nonatomic, strong) NSString * userAddrSec;         // 收货地址2
@property (nonatomic, strong) NSString * userAddThi;          // 收货地址3

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);



- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
