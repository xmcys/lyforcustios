//
//  RYKGoodsSortInfo.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-11-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYKGoodsSortInfo : NSObject

@property (nonatomic, strong) NSString * brandId;             // 类型名称
@property (nonatomic, strong) NSString * brandName;         // 类型值
@property (nonatomic, strong) NSString * serialNo;              // 类型值

- (void)parseData:(NSData *)data complete:(void(^)(NSArray * array))block;

@end
