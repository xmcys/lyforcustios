//
//  CustWebService.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKWebService.h"

static RYKCustInfo * USER; // 当前用户

@implementation RYKWebService

+ (void)setUser:(RYKCustInfo *)info
{
    USER = info;
}

+ (RYKCustInfo *)sharedUser
{
    return USER;
}

+ (NSString *)fileFullUrl:(NSString *)path
{
    NSRange range = [path rangeOfString:@"Resource/"];
    if (range.location == NSNotFound) {
        range = [path rangeOfString:@"resource/"];
    }
    if (range.location == NSNotFound) {
        return @"";
    }
    NSString * name = [path substringFromIndex:range.location + range.length];
    name = [name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [WEBSERVICE_FILE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", [path substringToIndex:range.location + range.length], name]];
}
// 物品图片具体路径
+ (NSString *)itemImageFullUrl:(NSString *)path
{
    return [WEBSERVICE_ITEM_IMAGE_URL stringByAppendingPathComponent:path];
}

// 即时聊天对象文件夹
+ (NSString *)msgTargetDir:(NSString *)msgTarget
{
    NSString* docsdir = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString* userDir = [docsdir stringByAppendingPathComponent:msgTarget];
    return userDir;
}

+ (NSString *)cmappVersionCheckUrl
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/custInterface/getApplicationEdition/appName/FZRYKWSQForCust/appType/%@", APP_TYPE]];
}

+ (NSString *)cmappLogInfoUrl:(NSString *)userId appType:(NSString *)appType version:(NSString *)version modCode:(NSString *)modCode operateId:(NSString *)operateId
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/custInterface/cmappAppLogInfo/userId/%@/appType/%@/versionNo/%@/modCode/%@/operateId/%@", userId, appType, version, modCode, operateId]];
}

+ (NSString *)custSignInUrl:(NSString *)userCode pwd:(NSString *)pwd
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/setup/getLogin/userCode/%@/userPwd/%@", userCode, pwd]];
}

+ (NSString *)savePasswordUrl
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"/custMobile/ws/rs/setup/getUpdatePwd"];
}

+ (NSString *)getUserMessage
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/theHomePage/getCustLicense/userId/%@", USER.userId]];
    
}

+ (NSString *)getOrderStatusUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/theHomePage/getOrderStatus/userId/%@", USER.userId]];
    
}

+ (NSString *)getUsMsgListById:(NSString *)msgId publishman:(NSString *)publishman
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/usMsgList/getUsMsgListById?msgId=%@&publishMan=%@",msgId, USER.userId]];


}

+ (NSString *)queryUsMsgList:(NSString *)msg
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/usMsgList/queryUsMsgList?msg=%@&publishMan=%@",msg,USER.userId]];
}

+ (NSString *)saveUsMsgList
{
    return [WEBSERVICE_HOST stringByAppendingString:@"/custMobile/ws/rs/usMsgList/saveUsMsgList"];
}

// 删除公告
+ (NSString *)deleteUsMsgList:(NSString *)msgId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/usMsgList/deleteUsMsgList?msgId=%@", msgId]];
}

+ (NSString *)updateUsMsgList
{
    return [WEBSERVICE_HOST stringByAppendingString:@"/custMobile/ws/rs/usMsgList/updateUsMsgList"];
}

// 商品类型url
+ (NSString *)getGoodsTypeUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/commodityManage/getSpecificationList"]];
}

// 商品筛选url
+ (NSString *)getGoodsListUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/commodityManage/getFilterTargetGoods"]];
}

// 商品上下架url
+ (NSString *)changeGoodsStateUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/commodityManage/getGoodsOffShelf"]];
}

// 商品规格url
+ (NSString *)getGoodsUnitUrl:(NSString *)typeId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/commodityManage/getSpecificationListUnit/brandType/%@", typeId]];
}

// 新增商品url
+ (NSString *)addNewGoodsUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/commodityManage/getSaveGoodsInfo"]];
}

// 修改商品url
+ (NSString *)modifyGoodsUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/commodityManage/getSaveModifyGoodsInfo"]];
}

// 保存商品促销信息url
+ (NSString *)saveGoodsOnSaleInfoUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/commodityManage/getSaveGoodsPromotionInfo"]];
}

// 查询商品信息in非烟库
+ (NSString *)queryGoodsInfoUrl:(NSString *)barCode
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/commodityManage/getProSaveGoodsInfo/userId/%@/barCode/%@", USER.userId, barCode]];
}

// 获取订单信息
+ (NSString *)getOrderFlow:(NSString *)flowCode withFlowStatus:(NSString *)flowStatus
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/orderManage/getVipOrderFlow/userId/%@/flowCode/%@/flowStatus/%@", USER.userId, flowCode, flowStatus]];
}

// 获取指定订单详情
+ (NSString *)getOrderDetail:(NSString *)orderId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/orderManage/getOrderDetail/orderId/%@", orderId]];
}

// 订单确认合并接口url
+ (NSString *)getOrderConfirmUrl:(NSString *)orderId withFlowCode:(NSString *)flowCode withTime:(NSString *)time
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/orderManage/upFlowStatus/orderId/%@/flowCode/%@/flowEstTime/%@", orderId, flowCode, time]];
}

// 获取订单追踪url
+ (NSString *)getOrderTrace:(NSString *)orderId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/orderManage/getOrderFlow/orderId/%@", orderId]];
}
    
+ (NSString *)getReadStoreInfo:(NSString *)userId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/storeInfo/getReadStoreInfo/userId/%@",userId]];
}

+ (NSString *)getSaveStoreInfo
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/storeInfo/getSaveStoreInfo"]];
}

+ (NSString *)getDeliveryContent
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/storeInfo/getDeliveryContent/userId/%@",USER.userId]];
}

+ (NSString *)getSaveDeliveryContent
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/storeInfo/getSaveDeliveryContent"]];
}

+ (NSString *)getMoreMessage:(NSString *)opinionFlag conFlag:(NSString *)conFlag dateFlag:(NSString *)dateFlag
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/MessageManage/getMoreMessage/userId/%@/opinionFlag/%@/conFlag/%@/dateFlag/%@", USER.userId,opinionFlag,conFlag,dateFlag]];
}

+ (NSString *)getDeleteMessage
{
    return [WEBSERVICE_HOST stringByAppendingString:@"/custMobile/ws/rs/MessageManage/getDeleteMessage"];
}

+ (NSString *)getSaveMessage
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/MessageManage/getSaveMessage"]];
}

+ (NSString *)getCustInfo
{
    return [WEBSERVICE_HOST stringByAppendingString:@"/custMobile/ws/rs/CustInfo/getCustInfo"];

}

+ (NSString *)getConsumerInfo
{
    return [WEBSERVICE_HOST stringByAppendingString:@"/custMobile/ws/rs/CustInfo/getConsumerInfo"];
}


+ (NSString *)getOrderId:(NSString *)userId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/CustInfo/getConsumerInfoOrder/vipUserId/%@",userId]];
}

+ (NSString *)getOrderIdDetail:(NSString *)userId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/CustInfo/getOrderId/vipUserId/%@",userId]];
}

+ (NSString *)getListRecords
{
    return [WEBSERVICE_HOST stringByAppendingString:@"/custMobile/ws/rs/CustInfo/getListRecords"];
}

// 销售信息url
+ (NSString *)getSalesOrderListUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/salesInfo/getSalesOrderList"]];
}

// 条码搜出销售信息url
+ (NSString *)getBarCodeSalesOrderListUrl
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/salesInfo/getTotalSum"]];
}

// 上传图片url
+ (NSString *)postImageUrl
{
    return [NSString stringWithFormat:@"%@/custMobile/servlet/FtpAction?fileId=%@", WEBSERVICE_IMAGE_HOST, USER.userId];
}

+ (NSString *)updateGoodsPrice:(NSString *)orderId brandId:(NSString *)brandId price:(NSString *)price
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/orderManage/upGoodsPrice/orderId/%@/brandId/%@/price/%@", orderId, brandId, price]];
}

+ (NSString *)upOrderPrice:(NSString *)orderId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/orderManage/upOrderPrice/orderId/%@", orderId]];
}

// 更新手机设备号
+ (NSString *)upDeviceToken:(NSString *)token
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/usUserInfo/upDeviceToken/userId/%@/deviceToken/%@", USER.userId, token]];
    NSString *encodedValue = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return encodedValue;
}

// 聊天上传图片url
+ (NSString *)chatPostImageUrl
{
    return [NSString stringWithFormat:@"%@/mobile-wyx/app/AppUploadImgServlet", @"http://210.13.192.100"];
}

+ (NSString *)getDictData
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"/custMobile/ws/rs/businessCircleTwo/getDictData"];
}

+ (NSString *)getDisplayImages:(NSString *)userId menuCode:(NSString *)menuCode
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getDisplayImages/userId/%@/menuCode/%@",userId,menuCode]];
}

+ (NSString *)getSavePic
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"/custMobile/ws/rs/businessCircleTwo/getSavePic"];
}

+ (NSString *)getRestoreDefault
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"/custMobile/ws/rs/businessCircleTwo/getRestoreDefault"];
}

+ (NSString *)getLinkAddress:(NSString *)userId
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getLinkAddress/userId/%@",userId]];
}

+ (NSString *)getBrandList:(NSString *)userId
{
    return [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getCountData/userId/%@",userId]];
}
    // 获取类别列表
+ (NSString *)getBrandTypeList
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getBrandList/userId/%@", USER.userId]];
    return url;
}

// 类别上移
+ (NSString *)typeMoveUpUrl
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getTypeMoveUp"]];
    return url;
}

// 类别下移
+ (NSString *)typeMoveDownUrl
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getTypeMoveDown"]];
    return url;
}

// 类别置顶
+ (NSString *)typeMoveToTopUrl
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getTypeTop"]];
    return url;
}

// 类别排序(上移，下移，置顶)通用post参数
+ (NSString *)typeSortParams:(NSString *)type serialNo:(NSString *)serialNo
{
    NSString * params = [NSString stringWithFormat:@"<Test><userId>%@</userId><brandType>%@</brandType><serialNo>%@</serialNo></Test>", USER.userId, type, serialNo];
    return params;
}

// 获取商品排序数据
+ (NSString *)getGoodsSortDisplay:(NSString *)isTabacco
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getGoodsDisplay/userId/%@/isTabacco/%@", USER.userId, isTabacco]];
    return url;
}

+ (NSString *)goodsMoveUpUrl
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getMoveUp"]];
    return url;
}

+ (NSString *)goodsMoveDownUrl
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getMoveDown"]];
    return url;
}

+ (NSString *)goodsMoveToTopUrl
{
    NSString *url = [WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/custMobile/ws/rs/businessCircleTwo/getTop"]];
    return url;
}

// 商品排序(上移，下移，置顶)通用post参数
+ (NSString *)goodsSortParams:(NSString *)brandId serialNo:(NSString *)serialNo isTabacco:(NSString *)isTabacco
{
    NSString * params = [NSString stringWithFormat:@"<Test><userId>%@</userId><brandId>%@</brandId><serialNo>%@</serialNo><isTabacco>%@</isTabacco></Test>", USER.userId, brandId, serialNo, isTabacco];
    return params;
}

@end
