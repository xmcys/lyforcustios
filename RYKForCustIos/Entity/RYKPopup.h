//
//  ConsPopup.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RYKPopup;

@protocol RYKPopupDelegate <NSObject>

- (void)popUp:(RYKPopup *)rykPopup popStateChanged:(BOOL)isShowing;
- (void)popUp:(RYKPopup *)rykPopup didSelecteInIndex:(NSInteger)index;

@end


@interface PopupItem : NSObject

@property (nonatomic, strong) NSString * key;
@property (nonatomic, strong) NSString * value;

@end

@interface RYKPopup : UIView

@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, strong) NSMutableArray * itemArray;
@property (nonatomic, weak) id<RYKPopupDelegate> delegate;

- (void)hidePopup;

@end
