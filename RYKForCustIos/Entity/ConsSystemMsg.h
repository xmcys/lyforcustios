//
//  CustSystemMsg.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ConsSystemMsg;

@protocol ConsSystemMsgDelegate <NSObject>

@optional

- (void)consSystemMsgDidFinishingParse:(ConsSystemMsg *)consSystemMsg;

@end

@interface ConsSystemMsg : NSObject

@property (nonatomic, strong) NSString * code;
@property (nonatomic, strong) NSString * msg;
@property (nonatomic, strong) NSString * extra;
@property (nonatomic, weak) id<ConsSystemMsgDelegate> delegate;

- (void)parseData:(NSData *)data;
- (void)parseData:(NSData *)data complete:(void(^)(ConsSystemMsg *msg))block;

@end
