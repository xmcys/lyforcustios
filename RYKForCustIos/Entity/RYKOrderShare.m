//
//  RYKOrderShare.m
//  RYKForCustIos
//
//  Created by hsit on 14-11-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKOrderShare.h"

@interface RYKOrderShare ()<NSXMLParserDelegate>


@end

@implementation RYKOrderShare

- (void)parseData:(NSData *)data complete:(void(^)(RYKOrderShare *item))block
{
    self.block = block;
    NSXMLParser *xml = [[NSXMLParser alloc] initWithData:data];
    xml.delegate = self;
    [xml parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.userId = [attributeDict objectForKey:@"userId"];
        self.userCode = [attributeDict objectForKey:@"userCode"];
        self.userName = [attributeDict objectForKey:@"userName"];
        self.shareUrl = [attributeDict objectForKey:@"shareUrl"];
        self.summary = [attributeDict objectForKey:@"summary"];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}



@end
