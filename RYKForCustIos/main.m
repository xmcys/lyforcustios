//
//  main.m
//  RYKForCustIos
//
//  Created by 宏超 陈 on 14-6-18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RYKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RYKAppDelegate class]));
    }
}
