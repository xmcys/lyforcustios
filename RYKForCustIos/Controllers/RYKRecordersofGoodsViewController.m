//
//  RYKRecordersofGoodsViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKRecordersofGoodsViewController.h"
#import "CTTableView.h"
#import "RYKWebService.h"
#import "RYKListRecords.h"
@interface RYKRecordersofGoodsViewController ()<UITableViewDataSource,UITableViewDelegate,CTTableViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerview;
@property (nonatomic, strong) IBOutlet UIView *bottomview;
@property (nonatomic, strong) CTTableView     *tableview;
@property (nonatomic, strong) NSMutableArray  *listarray;
@property (nonatomic, strong) IBOutlet UILabel *labelname;
@property (nonatomic, strong) IBOutlet UILabel *labelphone;
@property (nonatomic, strong) IBOutlet UILabel *labeladdr;
@property (nonatomic, strong) IBOutlet UILabel *labeldetal;
@property (nonatomic, strong) IBOutlet UILabel *labeldate;



@end

@implementation RYKRecordersofGoodsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithOrder:(NSString *)orderId
{
    if (self = [super init]) {
        self.orderId = orderId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.listarray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.title = @"消费明细查询";
    [super viewWillAppear:animated];
    if (self.tableview == nil) {
        self.bottomview.layer.borderColor = [[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] CGColor];
        self.bottomview.layer.borderWidth = 1;
        self.tableview = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tableview.backgroundColor = [UIColor clearColor];
        self.tableview.pullUpViewEnabled = NO;
        self.tableview.pullDownViewEnabled = NO;
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        self.tableview.ctTableViewDelegate = self;
        self.headerview.backgroundColor = [UIColor clearColor];
        [self.tableview setTableHeaderView:self.headerview];
        [self.view addSubview:self.tableview];
        [self loadData];
    }
}

- (void)loadData
{
    NSString *params = [NSString stringWithFormat:@"<Test><orderId>%@</orderId></Test>",self.orderId];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getListRecords] params:params delegate:self];
    [conn start];
}

#pragma mark - CTTableViewDelegate * Datasource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listarray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKRecordersofGoodsCell" owner:nil        options:nil];
        UIView *view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 0, tableView.frame.size.width-5*2, 80);
        [cell addSubview:view];
    }
    UILabel *labelname = (UILabel *)[cell viewWithTag:10];
    UILabel *labelprice = (UILabel *)[cell viewWithTag:11];
    UILabel *labelorder = (UILabel *)[cell viewWithTag:13];
   
    RYKListRecords *msg = (RYKListRecords *)[self.listarray objectAtIndex:indexPath.row];
    labelname.text = msg.brandName;
    labelprice.text = [NSString stringWithFormat:@"¥ %@/%@",msg.price,msg.brandUnit];
    labelorder.text = [NSString stringWithFormat:@"x %@",msg.qtyOrder];
    if (self.listarray.count > 0) {
        RYKListRecords *msg = (RYKListRecords *)[self.listarray objectAtIndex:0];
        self.labelname.text = msg.contactName;
        self.labelphone.text = msg.mobilePhone;
        self.labeladdr.text  = msg.receiveAddr;
        self.labeldetal.text = msg.orderDesc;
        NSString *str = [self.orderDate substringToIndex:19];
        self.labeldate.text = str;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - CTURLConnection Delegate

- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tableview reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    RYKListRecords *msg = [[RYKListRecords alloc] init];
    [msg parseData:data complete:^(NSArray *array){
        [self.indicator hide];
        [self.listarray removeAllObjects];
        [self.listarray addObjectsFromArray:array];
        [self.tableview reloadData];
    }];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
