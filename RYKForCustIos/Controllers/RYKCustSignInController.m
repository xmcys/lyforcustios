//
//  CustSignInController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-8.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKCustSignInController.h"
#import "RYKWebService.h"
#import "RYKCustMenuController.h"
#import "RYKAppDelegate.h"
#import "RYKXmppManager.h"
#import "RYKSqlLite.h"

@interface RYKCustSignInController () <UITextFieldDelegate, CTURLConnectionDelegate>

@property (nonatomic, strong) UIImageView * bg;
@property (nonatomic, strong) IBOutlet UIView * inputView;
@property (nonatomic, strong) IBOutlet UIImageView * inputBg;
@property (nonatomic, strong) IBOutlet UIButton * btnSignIn;
@property (nonatomic, strong) IBOutlet UITextField * userCode;
@property (nonatomic, strong) IBOutlet UITextField * pwd;
@property (nonatomic, strong) UIControl * cover;
@property (nonatomic, strong) CTIndicateView * indicator;

- (void)hideKeyboard;
- (IBAction)signIn:(UIButton *)sender;

@end

@implementation RYKCustSignInController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    self.inputBg.image = [[UIImage imageNamed:@"CustSignInInputBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(35, 26, 35, 26)];
    [self.btnSignIn setBackgroundImage:[[UIImage imageNamed:@"BtnGreenNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 3, 10, 3)] forState:UIControlStateNormal];
    [self.btnSignIn setBackgroundImage:[[UIImage imageNamed:@"BtnGreenHighLight"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 3, 10, 3)] forState:UIControlStateHighlighted];
    [self.view addSubview:self.inputView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.bg == nil) {
        self.bg = [[UIImageView alloc] initWithFrame:self.view.bounds];
        self.bg.contentMode = UIViewContentModeScaleAspectFill;
        self.bg.image = [UIImage imageNamed:@"CustSignInBg"];
        [self.view addSubview:self.bg];
        
        UIImageView *topIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CustSignInIcon"]];
        CGRect iconFrame = CGRectMake((self.view.bounds.size.width - topIcon.frame.size.width) / 2, 30, topIcon.frame.size.width, topIcon.frame.size.height);
        topIcon.frame = iconFrame;
        [self.view addSubview:topIcon];
    }
    
    if (self.cover == nil) {
        self.cover = [[UIControl alloc] initWithFrame:self.view.bounds];
        [self.cover addTarget:self action:@selector(hideKeyboard) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.cover];
        
        self.inputView.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - 45 - self.inputView.frame.size.height / 2);
    }
    
    [self.view bringSubviewToFront:self.inputView];
    
    self.indicator.backgroundTouchable = NO;
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSString * userCode = (NSString *)[defaults valueForKey:@"USER_CODE"];
    NSString * pwd = (NSString *)[defaults valueForKey:@"PASSWORD"];
    
    if (userCode != nil) {
        self.userCode.text = userCode;
        self.pwd.text = pwd;
    }
}

- (void)hideKeyboard
{
    [self.userCode resignFirstResponder];
    [self.pwd resignFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        self.inputView.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - 45 - self.inputView.frame.size.height / 2);
    }];
}

- (void)signIn:(UIButton *)sender
{
    [self hideKeyboard];
    
    if (self.indicator.showing) {
        return;
    }
    
    NSString * userCode = self.userCode.text;
    NSString * pwd = self.pwd.text;
    
    if (userCode.length != 12) {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"请输入12位许可证号" message:nil delegate:nil cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    if (pwd.length == 0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"密码不能为空" message:nil delegate:nil cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    self.indicator.text = @"正在验证..";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService custSignInUrl:userCode pwd:pwd] delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.inputView.frame.origin.y != 115 + self.inputView.frame.size.height / 2) {
        [UIView animateWithDuration:0.2 animations:^{
            self.inputView.center = CGPointMake(self.view.bounds.size.width / 2, 115 + self.inputView.frame.size.height / 2);
        }];
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userCode) {
        [self.pwd becomeFirstResponder];
    } else {
        [self.userCode resignFirstResponder];
        [self.pwd resignFirstResponder];
        [UIView animateWithDuration:0.2 animations:^{
            self.inputView.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - 45 - self.inputView.frame.size.height / 2);
        }];
        [self signIn:self.btnSignIn];
    }
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"验证失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    RYKCustInfo * info = [[RYKCustInfo alloc] init];
    [info parseData:data];
    if (info.userId == nil || [info.userId isEqualToString:@""]) {
        self.indicator.text = @"用户名或密码有误";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    } else {
        [self.indicator hide];
        info.userCode = self.userCode.text;
        [RYKWebService setUser:info];
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:self.userCode.text forKey:@"USER_CODE"];
        [defaults setValue:self.pwd.text forKey:@"PASSWORD"];
        [defaults synchronize];
        RYKCustMenuController * menuController = [[RYKCustMenuController alloc] initWithNibName:@"RYKCustMenuController" bundle:nil];
        [[UIApplication sharedApplication] delegate].window.rootViewController = menuController;
        [[RYKXmppManager sharedManager] connectToServer];
        [[RYKSqlLite shareManager] createMsgTable];
    }
    
}

@end
