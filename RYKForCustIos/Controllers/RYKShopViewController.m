//
//  RYKShopViewController.m
//  RYKForCustIos
//
//  Created by Arcesaka on 14-6-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKShopViewController.h"
#import "CTTableView.h"
#import "CTEditorController.h"
#import "RYKStoreInfo.h"
#import "RYKWebService.h"
#import "RYKDeliveryContent.h"
#import "ConsSystemMsg.h"
#import "ZbCommonFunc.h"
#import "RYKWmpViewController.h"


#define PROCESS_GET_STORE_MESSAGE 1
#define PROCESS_GET_DELIVER_MESSAGE 2
#define PROCESS_SAVE_STORE_MESSAGE 3
#define PROCESS_SAVE_DELIVER_MESSAGE 4

@interface RYKShopViewController ()<CTTableViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,CTEditorControllerDelegate,UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIView   *toptab;     //顶部tab栏
@property (strong, nonatomic) IBOutlet UIButton *shopbtn;    //店铺信息
@property (strong, nonatomic) IBOutlet UIButton *servicebtn; //送货服务
@property (strong, nonatomic) CTTableView       *tableview;  //店铺信息视图
@property (strong, nonatomic) UITextView        *textview;   //送货服务视图
@property (strong, nonatomic) UILabel           *defautlab;
@property (strong, nonatomic) NSArray           *shopNameArray;  //左标题数组
@property (strong, nonatomic) NSMutableArray    *msgArray;
@property (strong, nonatomic) RYKStoreInfo      *msg;

- (IBAction)onTabChanged:(UIButton *)btn;


@end

@implementation RYKShopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"店铺管理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.msgArray = [[NSMutableArray alloc] init];
    UIButton *btncommit = [UIButton buttonWithType:UIButtonTypeCustom];
    [btncommit setBackgroundImage:[UIImage imageNamed:@"提交按钮"] forState:UIControlStateNormal];
    btncommit.frame = CGRectMake(0, 0, 22, 22);
    [btncommit addTarget:self action:@selector(btncommitclick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnright = [[UIBarButtonItem alloc] initWithCustomView:btncommit];
    self.navigationItem.rightBarButtonItem = btnright;
    
    self.toptab.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TabBg"]];
    self.toptab.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.toptab.frame.size.height);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_DPGL, CONS_MODULE_DPGL, nil]];
}
- (void)btncommitclick:(UIButton *)btn
{
    [self.textview resignFirstResponder];
    if (self.indicator.showing) {
        return;
    }
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><delvIntro>%@</delvIntro></Test>",[RYKWebService sharedUser].userId,self.textview.text];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getSaveDeliveryContent] params:params delegate:self];
    conn.tag = PROCESS_SAVE_DELIVER_MESSAGE;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tableview == nil) {
        self.tableview = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.toptab.frame.size.height+10, self.view.bounds.size.width, self.view.bounds.size.height-self.toptab.frame.size.height-10)];
        self.tableview.pullUpViewEnabled = NO;
        self.tableview.backgroundColor = [UIColor clearColor];
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        self.tableview.ctTableViewDelegate = self;
        [self.view addSubview:self.tableview];
        [self loadStoreInfo];
        
        UIButton *pbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        pbtn.frame = CGRectMake(50, self.view.frame.size.height - 50, self.view.bounds.size.width -  50 * 2, 30);
        pbtn.backgroundColor = XCOLOR(4, 168, 117, 1);
        [pbtn setTitle:@"微信菜单管理" forState:UIControlStateNormal];
        pbtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [pbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        pbtn.layer.cornerRadius = 6;
        pbtn.layer.masksToBounds = YES;
        [pbtn addTarget:self action:@selector(manageClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pbtn];
    }
    if (self.textview == nil) {
        self.textview = [[UITextView alloc] initWithFrame:CGRectMake(5, self.toptab.bounds.size.height+10, self.view.bounds.size.width-5*2, 210)];
        self.textview.backgroundColor = [UIColor whiteColor];
        self.textview.font = [UIFont systemFontOfSize:15.0];
        self.textview.textColor = [UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1];
        self.textview.layer.cornerRadius = 5;
        self.textview.layer.borderWidth = 1;
        self.textview.delegate=self;
        self.textview.layer.borderColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1].CGColor;
        
        [self.view addSubview:self.textview];
        [self onTabChanged:self.shopbtn];
    }
}

- (void)manageClick:(UIButton *)btn
{
    RYKWmpViewController *controller = [[RYKWmpViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)loadStoreInfo
{
    if (self.indicator.showing) {
        return;
    }
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getReadStoreInfo:[RYKWebService sharedUser].userId] delegate:self];
    conn.tag = PROCESS_GET_STORE_MESSAGE;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

- (void)loadDeliverInfo
{
    if (self.indicator.showing) {
        return;
    }
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getDeliveryContent] delegate:self];
    conn.tag = PROCESS_GET_DELIVER_MESSAGE;
    [conn start];
    
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

- (IBAction)onTabChanged:(UIButton *)btn
{
    UIImage *img = [[UIImage imageNamed:@"on"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
    
    UIImage *clearImage = [ZbCommonFunc getImage:CGSizeMake(1, 1) withColor:[UIColor clearColor]];
    [self.shopbtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.shopbtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    [self.servicebtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.servicebtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    
    [self.shopbtn setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.shopbtn setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.servicebtn setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.servicebtn setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    
    
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    if (btn == self.shopbtn) {
        self.tableview.hidden = NO;
        self.textview.hidden = YES;
        self.navigationItem.rightBarButtonItem.customView.hidden = YES;
        [self.textview resignFirstResponder];
        [self loadStoreInfo];
    }
    else if (btn == self.servicebtn) {
        self.tableview.hidden = YES;
        self.textview.hidden = NO;
        self.navigationItem.rightBarButtonItem.customView.hidden = NO;
        [self loadDeliverInfo];
    }
}


#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView
{
    if (self.textview.text.length == 0) {
        self.defautlab.text = @"编辑送货服务说明";
    }
    else{
        self.defautlab.text = @"";
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textview resignFirstResponder];
}

//下拉刷新
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableview ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tableview ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate *DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = UITableViewCellSelectionStyleNone;
    NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKShopInfoCell" owner:nil options:nil];
    UIView *view = [elements objectAtIndex:0];
    view.frame = CGRectMake(0, 0, tableView.frame.size.width, 50);
    [cell addSubview:view];
    self.shopNameArray = [NSArray arrayWithObjects:@"商铺店名",@"固定电话",@"移动电话",@"地址",@"营业时间",@"商家介绍", nil];
    UILabel *infolab = (UILabel *)[cell viewWithTag:1];
    infolab.text = [self.shopNameArray objectAtIndex:indexPath.row];
    UILabel *detaillab = (UILabel *)[cell viewWithTag:2];
    if (self.msgArray.count > 0) {
        RYKStoreInfo *info = (RYKStoreInfo *)[self.msgArray objectAtIndex:0];
        if (indexPath.row == 0) {
            detaillab.text = info.userName;
        }else if (indexPath.row == 1) {
            detaillab.text = info.linkPhone;
       }else if (indexPath.row == 2) {
           detaillab.text = info.mobilPhone;
       }else if (indexPath.row == 3) {
           detaillab.text = info.userAddr;
       }else if (indexPath.row == 4) {
           detaillab.text = info.businesstime;
       }else if (indexPath.row == 5) {
           detaillab.text = info.storeIntro;
       }
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadStoreInfo];
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.msgArray.count <= 0) {
        return;
    }
    CTEditorController *controller = [[CTEditorController alloc] init];
    controller.delegate = self;
    self.msg = [self.msgArray objectAtIndex:0];
    controller.title = [self.shopNameArray objectAtIndex:indexPath.row];
    
    if (indexPath.row == 0) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        controller.view.tag = 0;
        [controller setDefaultValue:self.msg.userName];
    }
   else if (indexPath.row == 1) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        [controller setKeyboardType:UIKeyboardTypePhonePad];
        controller.view.tag = 1;
        [controller setDefaultValue:self.msg.linkPhone];
    }
   else if (indexPath.row == 2) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        [controller setKeyboardType:UIKeyboardTypeNumberPad];
        controller.view.tag = 2;
        [controller setDefaultValue:self.msg.mobilPhone];
    }
   else if (indexPath.row == 3) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        controller.view.tag = 3;
        [controller setDefaultValue:self.msg.userAddr];
    }
   else if (indexPath.row == 4) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        controller.view.tag = 4;
        [controller setDefaultValue:self.msg.businesstime];
    }
   else if (indexPath.row == 5) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextView;
        controller.view.tag = 5;
        [controller setDefaultValue:self.msg.storeIntro];
    }
    

}

#pragma mark - CTEditorController  Delegate
- (void)ctEditorController:(CTEditorController *)controller editDoneWithText:(NSString *)text
{
    if (self.indicator.showing) {
        return;
    }
    NSInteger tag = controller.view.tag;
    self.msg = [self.msgArray objectAtIndex:0];
    NSString *oriString = [NSString stringWithFormat:@"<Test><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilPhone>%@</mobilPhone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></Test>",[RYKWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,self.msg.mobilPhone,self.msg.userAddr,self.msg.businesstime,self.msg.storeIntro];
    switch (tag) {
        case SHOP_INFO_USERNAME:
          oriString = [NSString stringWithFormat:@"<Test><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilPhone>%@</mobilPhone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></Test>",[RYKWebService sharedUser].userId,text,self.msg.linkPhone,self.msg.mobilPhone,self.msg.userAddr,self.msg.businesstime,self.msg.storeIntro];
            break;
        case SHOP_INFO_LINKPHONE:
            oriString = [NSString stringWithFormat:@"<Test><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilPhone>%@</mobilPhone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></Test>",[RYKWebService sharedUser].userId,self.msg.userName,text,self.msg.mobilPhone,self.msg.userAddr,self.msg.businesstime,self.msg.storeIntro];
            break;
        case SHOP_INFO_MOBILPHONE:
            oriString = [NSString stringWithFormat:@"<Test><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilPhone>%@</mobilPhone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></Test>",[RYKWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,text,self.msg.userAddr,self.msg.businesstime,self.msg.storeIntro];
            break;
        case SHOP_INFO_USERADDR:
          oriString = [NSString stringWithFormat:@"<Test><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilPhone>%@</mobilPhone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></Test>",[RYKWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,self.msg.mobilPhone,text,self.msg.businesstime,self.msg.storeIntro];
            break;
        case SHOP_INFO_BUSSINESSTIME:
            oriString = [NSString stringWithFormat:@"<Test><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilPhone>%@</mobilPhone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></Test>",[RYKWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,self.msg.mobilPhone,self.msg.userAddr,text,self.msg.storeIntro];
                          break;
        case SHOP_INFO_STOREINTRO:
            oriString = [NSString stringWithFormat:@"<Test><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilPhone>%@</mobilPhone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></Test>",[RYKWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,self.msg.mobilPhone,self.msg.userAddr,self.msg.businesstime,text];
            break;
        default:
            break;
    }
    self.indicator.text = @"正在提交..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getSaveStoreInfo] params:oriString delegate:self];
    conn.tag = PROCESS_SAVE_STORE_MESSAGE;
    [conn start];
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tableview reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_STORE_MESSAGE) {
            RYKStoreInfo * msg = [[RYKStoreInfo alloc] init];
            [msg parseData:data complete:^(NSArray *array){
                [self.indicator hide];
                [self.msgArray removeAllObjects];
                [self.msgArray addObjectsFromArray:array];
                [self.tableview reloadData];

        }];
    } else if(connection.tag == PROCESS_GET_DELIVER_MESSAGE){
            RYKDeliveryContent *msg = [[RYKDeliveryContent alloc] init];
            [msg parseData:data complete:^(RYKDeliveryContent *msg){
                [self.indicator hide];
                self.textview.text = msg.delvIntro;
        }];
    
    } else if(connection.tag == PROCESS_SAVE_DELIVER_MESSAGE){
            ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
            [msg parseData:data];
            if ([msg.code isEqualToString:@"1"]) {
                self.indicator.text = @"修改成功";
                [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
                return;
        }
    } else if(connection.tag == PROCESS_SAVE_STORE_MESSAGE){
            ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
            [msg parseData:data];
            if ([msg.code isEqualToString:@"1"]) {
                [self.indicator hide];
                self.indicator.text = @"修改成功";
                [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
                self.indicator.showing = NO;
                [self loadStoreInfo];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
}




@end
