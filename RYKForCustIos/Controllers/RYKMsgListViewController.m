//
//  RYKMsgListViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKMsgListViewController.h"
#import "CTTableView.h"
#import "RYKWebService.h"
#import "RYKMsg.h"
#import "RYKPopup.h"



#define PROCESS_GET_LISTDATA 1
#define PROCESS_GET_TYPEDATA 2

@interface RYKMsgListViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,CTTableViewDelegate,RYKPopupDelegate>
@property (nonatomic, strong) IBOutlet UIView *toptab;   //按钮视图
@property (nonatomic, strong) CTTableView     *tv;
@property (nonatomic, strong) NSMutableArray  *msgArray;
@property (nonatomic, strong) RYKPopup        *typePopup;    //类型下拉
@property (nonatomic, strong) RYKPopup        *statePopup;    //状态下拉
@property (nonatomic, strong) RYKPopup        *timePopup;    //时间下拉



@end

@implementation RYKMsgListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.msgArray = [[NSMutableArray alloc] init];
    self.toptab.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.toptab.bounds.size.height);
    self.toptab.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TabBg"]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_LY, CONS_MODULE_LY, nil]];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"留言信息管理";
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.toptab.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height-self.toptab.bounds.size.height-10)];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
        [self loadData];
            }
    if (self.typePopup ==  nil) {
        self.typePopup = [[RYKPopup alloc] initWithFrame:CGRectMake(0, self.toptab.frame.size.height/3-10, self.view.frame.size.width/3, 35)];
        self.typePopup.delegate = self;
        [self.view addSubview:self.typePopup];
        
        self.statePopup = [[RYKPopup alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, self.toptab.frame.size.height/3-10, self.view.frame.size.width/3, 35)];
        self.statePopup.delegate = self;
        [self.view addSubview:self.statePopup];

        self.timePopup = [[RYKPopup alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3*2, self.toptab.frame.size.height/3-10, self.view.frame.size.width/3, 20)];
        self.timePopup.delegate = self;
        [self.view addSubview:self.timePopup];
        
        UIImageView * sep1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.typePopup.frame.size.width - 1, (self.toptab.frame.size.height - 25) / 2, 2, 25)];
        sep1.image = [[UIImage imageNamed:@"分割线"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self.view addSubview:sep1];
        
        UIImageView * sep2 = [[UIImageView alloc] initWithFrame:CGRectMake(self.statePopup.frame.origin.x + self.statePopup.frame.size.width - 1, (self.toptab.frame.size.height - 25) / 2 , 2, 25)];
        sep2.image = [[UIImage imageNamed:@"分割线"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self.view addSubview:sep2];
        
        PopupItem *type01 = [[PopupItem alloc] init];
        type01.key = @"-1";
        type01.value = @"不限类型";
        PopupItem *type02 = [[PopupItem alloc] init];
        type02.key = @"1";
        type02.value = @"需求";
        PopupItem *type03 = [[PopupItem alloc] init];
        type03.key = @"2";
        type03.value = @"建议";
        PopupItem *type04 = [[PopupItem alloc] init];
        type04.key = @"3";
        type04.value = @"咨询";
        NSMutableArray *typeArray = [NSMutableArray arrayWithObjects:type01, type02,type03,type04, nil];
        [self.typePopup setItemArray:typeArray];
        
        PopupItem *state01 = [[PopupItem alloc] init];
        state01.key = @"-1";
        state01.value = @"不限状态";
        PopupItem *state02 = [[PopupItem alloc] init];
        state02.key = @"0";
        state02.value = @"未回复";
        PopupItem *state03 = [[PopupItem alloc] init];
        state03.key = @"1";
        state03.value = @"已回复";
        NSMutableArray *stateArray = [NSMutableArray arrayWithObjects:state01, state02,state03, nil];
        [self.statePopup setItemArray:stateArray];
        [self.statePopup setSelectedIndex:1];
        PopupItem *time01 = [[PopupItem alloc] init];
        time01.key = @"0";
        time01.value = @"日期降序";
        PopupItem *time02 = [[PopupItem alloc] init];
        time02.key = @"1";
        time02.value = @"日期升序";
        NSMutableArray *timeArray = [NSMutableArray arrayWithObjects:time01, time02, nil];
        [self.timePopup setItemArray:timeArray];
        [self.timePopup setSelectedIndex:0];
    }
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getMoreMessage:@"-1" conFlag:@"0" dateFlag:@"0"] delegate:self];
    conn.delegate = self;
    conn.tag = PROCESS_GET_LISTDATA;
    [conn start];
    
    return;
}

- (void)reloadMsgList
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    NSString *typeValue = ((PopupItem *)[self.typePopup.itemArray objectAtIndex:self.typePopup.selectedIndex]).key;
    NSString *statevalue = ((PopupItem *)[self.statePopup.itemArray objectAtIndex:self.statePopup.selectedIndex]).key;
    NSString *timevalue = ((PopupItem *)[self.timePopup.itemArray objectAtIndex:self.timePopup.selectedIndex]).key;

    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getMoreMessage:typeValue conFlag:statevalue dateFlag:timevalue] delegate:self];
    conn.tag = PROCESS_GET_TYPEDATA;
    [conn start];
    return;
}

#pragma mark - RefreshDelegate

- (void)RefreshGetMoreMsg:(BOOL)Refresh
{
    if (Refresh == YES) {
        [self reloadMsgList];
        NSLog(@"刷新成功");
    }
    else{
        return;
    }
}

#pragma mark - RYKPopup Delegate
- (void)popUp:(RYKPopup *)rykPopup didSelecteInIndex:(NSInteger)index
{
    [self reloadMsgList];
    return;
}

- (void)popUp:(RYKPopup *)rykPopup popStateChanged:(BOOL)isShowing
{
    if (isShowing) {
        if (rykPopup == self.typePopup) {
            [self.statePopup hidePopup];
            [self.timePopup hidePopup];
        } else if (rykPopup == self.statePopup){
            [self.typePopup hidePopup];
            [self.timePopup hidePopup];
        } else {
            [self.typePopup hidePopup];
            [self.statePopup hidePopup];
        }
    }
    return;
}

#pragma mark - CTTableViewDelegate  *  Datasource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self reloadMsgList];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.msgArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RYKMsg *msg = (RYKMsg *)[self.msgArray objectAtIndex:indexPath.row];
    if ([msg.opinionFlag isEqualToString:@"已回复"]) {
        return 175;
    }
    return 105;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKMsgListCell" owner:nil        options:nil];
        UIView *view = [elements objectAtIndex:0];
        view.frame = CGRectMake(0, 5, tableView.frame.size.width, 100);
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell addSubview:view];
    }
    UILabel *timelab = (UILabel *)[cell viewWithTag:10];
    UILabel *replylab = (UILabel *)[cell viewWithTag:11];
    UIImageView *icon = (UIImageView *)[cell viewWithTag:12];
    UILabel *namelab = (UILabel *)[cell viewWithTag:13];
    UILabel *contlab = (UILabel *)[cell viewWithTag:14];
    UIImageView *img = (UIImageView *)[cell viewWithTag:20];
    UIView *bottomview = (UIView *)[cell viewWithTag:30];
    UIView *backview = (UIView *)[cell viewWithTag:31];
    UILabel *labanswerdate = (UILabel *)[cell viewWithTag:33];
    UILabel *labanswercont = (UILabel *)[cell viewWithTag:34];
    UIImageView *line = (UIImageView *)[cell viewWithTag:35];
    backview.layer.cornerRadius =5;
    backview.layer.masksToBounds = YES;
    img.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
    RYKMsg *msg = (RYKMsg *)[self.msgArray objectAtIndex:indexPath.row];
    if ([msg.opinionFlag isEqualToString:@"已回复"]) {
        replylab.text = @"已回复";
        replylab.textColor = [UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1];
        icon.hidden = YES;
        line.hidden = YES;
        bottomview.hidden = NO;
        NSString *dateStr = [msg.answerDate substringToIndex:19];
        labanswerdate.text = dateStr;
        labanswercont.text = msg.answerCont;
    }else if ([msg.opinionFlag isEqualToString:@"未回复"]){
        replylab.text = @"回复";
        replylab.textColor = [UIColor colorWithRed:254.0/255.0 green:161.0/255.0 blue:30.0/255.0 alpha:1];
        icon.hidden = NO;
        bottomview.hidden = YES;
        line.hidden = NO;
    }
    timelab.text = msg.opinionDate;
    namelab.text = [NSString stringWithFormat:@"%@ :",msg.userName];
    contlab.text = msg.opinionCont;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RYKMsg *msg = [self.msgArray objectAtIndex:indexPath.row];
    RYKMsgReplyViewController *controller = [[RYKMsgReplyViewController alloc] init];
    controller.delegate = self;
    controller.msg = msg;
    [self.navigationController pushViewController:controller animated:YES];
    
    
//    NSString *typeValue = ((PopupItem *)[self.typePopup.itemArray objectAtIndex:self.typePopup.selectedIndex]).key;
//    NSString *statevalue = ((PopupItem *)[self.statePopup.itemArray objectAtIndex:self.statePopup.selectedIndex]).key;
//    NSString *timevalue = ((PopupItem *)[self.timePopup.itemArray objectAtIndex:self.timePopup.selectedIndex]).key;
//    NSLog(@"%@",[RYKWebService getMoreMessage:typeValue conFlag:statevalue dateFlag:timevalue]);
}

#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}
#pragma mark -  CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_LISTDATA) {
    RYKMsg *msg = [[RYKMsg alloc] init];
    [msg parseData:data complete:^(NSArray *array){
        [self.indicator hide];
        [self.msgArray removeAllObjects];
        [self.msgArray addObjectsFromArray:array];
        [self.tv reloadData];
    }];
    }
   else if (connection.tag == PROCESS_GET_TYPEDATA) {
        RYKMsg *msg = [[RYKMsg alloc] init];
        [msg parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.msgArray removeAllObjects];
            [self.msgArray addObjectsFromArray:array];
            [self.tv reloadData];
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
