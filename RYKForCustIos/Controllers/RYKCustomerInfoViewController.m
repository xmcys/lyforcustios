//
//  RYKCustomerInfoViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-20.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKCustomerInfoViewController.h"
#import "CTTableView.h"
#import "RYKCustomerDetailInfoViewController.h"
#import "RYKWebService.h"
#import "RYKCustomer.h"
#define PROCESS_POST_CUSTINFO  1
#define PROCESS_GET_CUSTINFO   2

@interface RYKCustomerInfoViewController ()<UITableViewDataSource,UITableViewDelegate,CTTableViewDelegate,UITextFieldDelegate,UIScrollViewDelegate>
@property (nonatomic, strong) IBOutlet UIView  *headerView;     //搜索视图
@property (nonatomic, strong) IBOutlet UIView  *searchView;
@property (nonatomic, strong) CTTableView      *tableView;
@property (nonatomic, strong) NSMutableArray   *customerArray;
@property (nonatomic, strong) IBOutlet UITextField    *searchText;  //搜索框

- (IBAction)searchclick:(id)sender;

@end

@implementation RYKCustomerInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"顾客信息管理";
    }
    return self;
}

- (IBAction)searchclick:(id)sender
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    [self.searchText resignFirstResponder];
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><keyWord>%@</keyWord></Test>",[RYKWebService sharedUser].userId,self.searchText.text];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getCustInfo] params:params delegate:self];
    conn.tag = PROCESS_POST_CUSTINFO;
    [conn start];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.customerArray = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_GKGL, CONS_MODULE_GKGL, nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.searchView.layer.cornerRadius = 5.0;
    if (self.tableView == nil) {
        self.tableView = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.bounds.size.height, self.view.frame.size.width, self.view.frame.size.height- self.headerView.bounds.size.height-10)];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.pullUpViewEnabled = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.ctTableViewDelegate = self;
        [self.view addSubview:self.tableView];
        [self loadData];
    }
    self.searchText.returnKeyType = UIReturnKeySearch;
    self.searchText.delegate = self;
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><keyWord></keyWord></Test>",[RYKWebService sharedUser].userId];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getCustInfo] params:params delegate:self];
    conn.tag = PROCESS_GET_CUSTINFO;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    [self.tableView reloadData];
    return;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.searchText resignFirstResponder];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.searchText resignFirstResponder];
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><keyWord>%@</keyWord></Test>",[RYKWebService sharedUser].userId,self.searchText.text];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getCustInfo] params:params delegate:self];
    conn.tag = PROCESS_POST_CUSTINFO;
    [conn start];
    return YES;
}
//下拉刷新
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableView ctTableViewDidScroll];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tableView ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate *DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 138;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.customerArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"RYKCustomerCell" owner:nil options:nil];
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 8, self.tableView.bounds.size.width-5*2, 130);
        view.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        
        UIView *lineview = (UIView *)[cell viewWithTag:20];
        lineview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
        [cell addSubview:view];
    }
    UILabel *labname = (UILabel *)[cell viewWithTag:1];
    UILabel *labbirth = (UILabel *)[cell viewWithTag:2];
    UILabel *labphone = (UILabel *)[cell viewWithTag:3];
    UILabel *labresign = (UILabel *)[cell viewWithTag:4];
    RYKCustomer *info = (RYKCustomer *)[self.customerArray objectAtIndex:indexPath.row];
    //截取日期字符串
    if (info.birthday.length >= 10) {
        NSString *str = [info.birthday substringToIndex:11];
        NSString *reg = [info.regisTime substringToIndex:11];
        labbirth.text = str;
        labresign.text = reg;
    }
    //判断时期注册日期是否是今天
    NSString *reg = [info.regisTime substringToIndex:11];
    NSDate *date = [NSDate date];
    NSString  *str1 = [NSString stringWithFormat:@"%@",date];
    NSString  *str2 = [str1 substringToIndex:11];
    if ([str2 isEqualToString:reg]) {
        labname.textColor = [UIColor orangeColor];
    }else {
        labname.textColor = [UIColor blackColor];
    }
    labname.text = info.userName;
    labphone.text = info.mobilphone;
    //判断字符串为空不显示
    if ([info.userName isEqualToString:@"null"] ) {
        labname.text = @"";
    }else if ([info.birthday isEqualToString:@"null"] ) {
        labbirth.text = @"";
    }else if ([info.mobilphone isEqualToString:@"null"] ) {
        labphone.text = @"";
    }else if ([info.regisTime isEqualToString:@"null"] ) {
        labresign.text = @"";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RYKCustomer *msg = [self.customerArray objectAtIndex:indexPath.row];
    RYKCustomerDetailInfoViewController *controller = [[RYKCustomerDetailInfoViewController alloc] init];
    controller.msg = msg;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tableView reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_CUSTINFO) {
        RYKCustomer *info = [[RYKCustomer alloc] init];
        [info parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.customerArray removeAllObjects];
            [self.customerArray addObjectsFromArray:array];
            [[RYKWebService sharedUser] updateCustomerDict:array];
            [self.tableView reloadData];
            UILabel *custnum = (UILabel *)[self.headerView viewWithTag:30];
            custnum.text = [NSString stringWithFormat:@"%d",self.customerArray.count];
            UILabel *focus = (UILabel *)[self.headerView viewWithTag:20];
            focus.text = [RYKWebService sharedUser].userTotalNum;
    }];
    }else if (connection.tag == PROCESS_POST_CUSTINFO){
        RYKCustomer *info = [[RYKCustomer alloc] init];
        [info parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.customerArray removeAllObjects];
            [self.customerArray addObjectsFromArray:array];
            [self.tableView reloadData];
        }];
         }
}



@end
