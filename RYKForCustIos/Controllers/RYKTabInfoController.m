//
//  RYKTabInfoController.m
//  RYKForCustIos
//
//  Created by 宏超 陈 on 14-6-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKTabInfoController.h"
#import "RYKPublicViewController.h"
#import "RYKShopViewController.h"
#import "RYKCustomerInfoViewController.h"
#import "RYKSaleInfoViewController.h"
#import "CTTableView.h"
#import "RYKMsgListViewController.h"
#import "RYKWebService.h"
#import "RYKMsg.h"
#import "ConsSystemMsg.h"
#import "CXButton.h"


#define PROCESS_GET_USER_MESSAGE  1
#define PROCESS_POST_REPLY_MESSAGE 2


@interface RYKTabInfoController ()<CTTableViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIView   *headerView;    //按钮父视图
@property (strong, nonatomic) IBOutlet UIButton *btnPublic;     //公告管理
@property (strong, nonatomic) IBOutlet UIButton *btnShop;       //店铺管理
@property (strong, nonatomic) IBOutlet UIButton *btnCif;        //顾客信息管理
@property (strong, nonatomic) IBOutlet UIButton *btnSim;        //销售信息管理
@property (strong, nonatomic) IBOutlet UILabel  *msgNumlab;     //留言数
@property (strong, nonatomic) CTTableView       *tableView;     //留言列表
@property (strong, nonatomic) IBOutlet UIView   *fastReplyView; //快速回复视图
@property (strong, nonatomic) UIView            *fullView;      //全屏View
@property (strong, nonatomic) IBOutlet UITextView *textView;    //回复留言文本
@property (strong, nonatomic) NSMutableArray    *msgArray;      //留言数组
@property (strong, nonatomic) IBOutlet UIView   *msgEntryView;  //留言管理入口
@property (strong, nonatomic) IBOutlet UIImageView *line;
@property (strong, nonatomic) NSString *opinionSeq;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btn1x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btn2x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btn3x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btn4x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnwidthx;


- (IBAction)btnPubClick:(id)sender;
- (IBAction)btnShpClick:(id)sender;
- (IBAction)btnCifClick:(id)sender;
- (IBAction)btnSimClick:(id)sender;

- (IBAction)btnCancle:(id)sender;
- (IBAction)btnSure:(id)sender;

- (void)updateViewConstraints;
- (void)autoArrangeContraints:(NSArray *)constraintArray width:(CGFloat)width;

@end

@implementation RYKTabInfoController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"信息管理" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:116.0/255.0 alpha:1] image:[UIImage imageNamed:@"TabInfo"] selectedImage:[UIImage imageNamed:@"TabInfoHighlighted"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.msgArray = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_XXGL, CONS_MODULE_XXGL, nil]];

}

- (void)updateViewCons
{
    [self autoArrangeContraints:@[self.btn1x,
                                  self.btn2x,
                                  self.btn3x,
                                  self.btn4x] width:self.btnwidthx.constant];
}

- (void)autoArrangeContraints:(NSArray *)constraintArray width:(CGFloat)width
{
    CGFloat step = (self.view.frame.size.width - constraintArray.count * width) / (constraintArray.count + 1);
    for (int i = 0; i < constraintArray.count; i ++) {
        NSLayoutConstraint *constraint = constraintArray[i];
        constraint.constant = width * i + step * (1 + i);
    }
}


-(IBAction)btnPubClick:(id)sender
{
    RYKPublicViewController *controller = [[RYKPublicViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)btnShpClick:(id)sender
{
    RYKShopViewController *controller = [[RYKShopViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)btnCifClick:(id)sender
{
    RYKCustomerInfoViewController *controller = [[RYKCustomerInfoViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)btnSimClick:(id)sender
{
    RYKSaleInfoViewController *controller = [[RYKSaleInfoViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = @"信息管理";
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    self.line.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
    if (self.tableView == nil) {
        self.tableView = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.pullUpViewEnabled = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.ctTableViewDelegate = self;
        [self.tableView setTableHeaderView:self.headerView];
        [self.view addSubview:self.tableView];
    }
    [self updateViewCons];
    [self enterMsg];
    [self loadData];
}


- (void)enterMsg
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(msgListcontroller:)];
    tap.numberOfTapsRequired = 1;
    [self.msgEntryView addGestureRecognizer:tap];
}

- (void)msgListcontroller:(UIView *)msgView
{
    RYKMsgListViewController *controller = [[RYKMsgListViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getMoreMessage:@"-1" conFlag:@"0" dateFlag:@"0"] delegate:self];
    conn.delegate = self;
    conn.tag = PROCESS_GET_USER_MESSAGE;
    [conn start];
    return;
}

- (void)replyMsg:(CXButton *)replybtn
{
    NSIndexPath *path = [NSIndexPath indexPathForRow:replybtn.index inSection:0];
    RYKMsg *item = (RYKMsg *)[self.msgArray objectAtIndex:[path row]];
    self.opinionSeq = [NSString stringWithFormat:@"%@",item.opinionSeq];
    [self.textView becomeFirstResponder];

    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    self.fullView = [[UIView alloc] initWithFrame:window.bounds];
    self.fullView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:0.5];
    self.fastReplyView.frame = CGRectMake((self.tableView.frame.size.width - self.fastReplyView.frame.size.width) / 2, (self.tableView.bounds.size.height-235) / 2, 220, 128);
    self.fastReplyView.backgroundColor = [UIColor whiteColor];
    self.fastReplyView.layer.borderWidth = 1;
    self.fastReplyView.layer.borderColor = [[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] CGColor];
    self.fastReplyView.layer.masksToBounds = YES;
    self.fastReplyView.layer.cornerRadius = 10;
    [self.fullView addSubview:self.fastReplyView];
    [window addSubview:self.fullView];
}

- (IBAction)btnCancle:(id)sender
{
    self.textView.text = NULL;
    [self.fullView removeFromSuperview];
}

- (IBAction)btnSure:(id)sender{
    if (self.textView.text.length == 0) {
        self.indicator.text = @"回复的留言不能为空";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];

        return;
    }
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><opinionSeq>%@</opinionSeq><answerPerson>%@</answerPerson><answerCont>%@</answerCont></Test>", [RYKWebService sharedUser].userId, self.opinionSeq, @"", self.textView.text];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getSaveMessage] params:params delegate:self];
    conn.tag = PROCESS_POST_REPLY_MESSAGE;
    [conn start];
    
    self.textView.text = NULL;
    [self.fullView removeFromSuperview];
}

//下拉刷新
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableView ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tableView ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate *DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.msgArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell ==  nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"RYKMsgCell" owner:nil options:nil] lastObject];
        view.frame = CGRectMake(0, 0, tableView.frame.size.width, 120);
        [cell addSubview:view];
    }
    
    UILabel *timelab = (UILabel *)[cell viewWithTag:1];
    UILabel *namelab = (UILabel *)[cell viewWithTag:2];
    UILabel *contentlab = (UILabel *)[cell viewWithTag:3];
    CXButton *replybtn = (CXButton *)[cell viewWithTag:4];
    UIView  *lineview = (UIView *)[cell viewWithTag:20];
    UIView  *bottomline = (UIView *)[self.headerView viewWithTag:1000];
    //图片拉伸
    UIImage *img = [[UIImage imageNamed:@"btn_1"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    UIImage *img2 = [[UIImage imageNamed:@"btn_2"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    replybtn.index = indexPath.row;
    [replybtn setBackgroundImage:img forState:UIControlStateNormal];
    [replybtn setBackgroundImage:img2 forState:UIControlStateHighlighted];
    [replybtn addTarget:self action:@selector(replyMsg:) forControlEvents:UIControlEventTouchUpInside];
    lineview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
    lineview.hidden = YES;
    bottomline.hidden = YES;
    if (self.msgArray.count <= 0) {
        lineview.hidden = YES;
        bottomline.hidden = NO;
    } else {
        lineview.hidden = NO;
        bottomline.hidden = YES;
    }
    
    RYKMsg *item = (RYKMsg *)[self.msgArray objectAtIndex:indexPath.row];
    timelab.text = item.opinionDate;
    namelab.text = item.userName;
    contentlab.text = item.opinionCont;
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tableView reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_USER_MESSAGE) {
         RYKMsg * msg = [[RYKMsg alloc] init];
         [msg parseData:data complete:^(NSArray *array){
             [self.indicator hide];
             [self.msgArray removeAllObjects];
             [self.msgArray addObjectsFromArray:array];
             self.msgNumlab.text = [NSString stringWithFormat:@"%d",self.msgArray.count];
             [self.tableView reloadData];
        }];
    }else if(connection.tag == PROCESS_POST_REPLY_MESSAGE){
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            [self.indicator hide];
            self.indicator.text = @"回复成功";
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1.2 complete:^(void){
                [self.indicator hide];
                self.indicator.showing = NO;
                [self loadData];
            }];
            }
    }

}
@end
