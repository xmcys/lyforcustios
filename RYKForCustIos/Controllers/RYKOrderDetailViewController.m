//
//  RYKOrderDetailViewController.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-30.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKOrderDetailViewController.h"
#import "RYKWebService.h"
#import "RYKOrderItem.h"
#import "ConsSystemMsg.h"
#import "RYKOrderTrace.h"
#import "ZbCommonFunc.h"
#import "CXButton.h"

#define PROCESS_GET_DETAILINFO 1
#define PROCESS_ORDER_CONFIRM 2
#define PROCESS_ORDER_DELIVPACK_CONFIRM 3
#define PROCESS_ORDER_RECV_CONFIRM 4
#define PROCESS_ORDER_TRACE 5
#define PROCESS_UPDATE_ORDER_SUM 6
#define PROCESS_UPDATE_ORDER_GOODSPRICE 100

@interface RYKOrderDetailViewController ()<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, CTURLConnectionDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UITableView *tv;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UILabel *labName;
@property (nonatomic, strong) IBOutlet UILabel *labTel;
@property (nonatomic, strong) IBOutlet UILabel *labAddr;
@property (nonatomic, strong) IBOutlet UILabel *labDes;
@property (nonatomic, strong) IBOutlet UIView *headerViewLine;
@property (nonatomic, strong) IBOutlet UIImageView *sendGoods;     // 送货上门
@property (nonatomic, strong) IBOutlet UIImageView *getGoods;      // 上门自取
@property (nonatomic, strong) IBOutlet UIView *sendBottomView;     // 送货上门底部view
@property (nonatomic, strong) IBOutlet UIView *getBottomView;      // 上门自取底部view
@property (nonatomic, strong) IBOutlet UIView *recvBottomView;     // 收货底部view
@property (nonatomic, strong) IBOutlet UIView *completeBottomView; // 已完成底部view
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
@property (nonatomic, strong) IBOutlet UIView *tvBottom;
@property (nonatomic, strong) IBOutlet UITextField *tfSendTime;
@property (nonatomic, strong) IBOutlet UIButton *btnSendConfirm;
@property (nonatomic, strong) IBOutlet UIButton *btnGetConfirm;
@property (nonatomic, strong) IBOutlet UIButton *btnRecvConfirm;

@property (nonatomic, strong) IBOutlet UIView *editGoodsView;      //编辑商品视图
@property (nonatomic, strong) UIView          *fullView;           //全屏视图
@property (nonatomic, strong) UITextField     *goodsField;         //商品单价
@property (nonatomic, strong) UILabel         *labGoodsName;       //商品名称
@property (nonatomic, strong) UILabel         *labGoodsPrice;      //商品原价
@property (nonatomic, strong) UILabel         *labGoodsUnit;       //商品单位
@property (nonatomic, strong) NSIndexPath     *path;
@property (nonatomic, strong) UILabel         *labSumMoney;
@property (nonatomic, strong) RYKOrderItem    *tmpItem;            //临时对象
@property (nonatomic) BOOL flag;

@property (nonatomic, strong) UIControl * cover;
@property (nonatomic, strong) NSMutableArray *itemsArray;          // 商品列表
@property (nonatomic, strong) NSMutableArray *itemsTmpArray;          // 商品临时列表
@property (nonatomic) NSInteger bottomHeight;
@property (nonatomic) NSInteger modifyPriceItemCnt;
@property (nonatomic) NSInteger modifySuccessCnt;

- (void)drawBaseInfo;
- (void)drawSumMoney;
- (void)drawTraceInfo:(RYKOrderTrace *)curTrace;
- (void)loadOrderDetailData;
- (void)loadOrderTraceData;
- (void)delivPackGoodsConfirmData;
- (void)textFieldUp:(NSInteger)height;
- (void)textFieldDown:(NSInteger)height;
-(IBAction)btnSendConfirmClicked:(UIButton *)btn;
-(IBAction)btnGetConfirmClicked:(UIButton *)btn;
-(IBAction)btnRecvConfirmClicked:(UIButton *)btn;
- (IBAction)btnCancleEditGoods:(id)sender;
- (IBAction)btnSureEditGoods:(id)sender;
- (void)coverOnClicked;

@end

@implementation RYKOrderDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"订单详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.itemsArray = [[NSMutableArray alloc] init];
    self.itemsTmpArray = [[NSMutableArray alloc] init];
    
    if ([self.curOrder.flowCode isEqualToString:@"FINISH"]) {
        [self.view addSubview:self.completeBottomView];
        self.bottomHeight = self.completeBottomView.frame.size.height;
    } else if ([self.curOrder.flowCode isEqualToString:@"STORE_CONFIRM"]) {
        if ([self.curOrder.recvType isEqualToString:@"1"]) {
            [self.view addSubview:self.sendBottomView];
            self.bottomHeight = self.sendBottomView.frame.size.height;
        } else {
            [self.view addSubview:self.getBottomView];
            self.bottomHeight = self.getBottomView.frame.size.height;
        }
    } else {
        [self.view addSubview:self.recvBottomView];
        self.bottomHeight = self.recvBottomView.frame.size.height;
    }
    
    UIImage *img = [[UIImage imageNamed:@"btn_1"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 5, 3, 5)];
    UIImage *imgHl = [[UIImage imageNamed:@"btn_2"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 5, 3, 5)];
    [self.btnGetConfirm setBackgroundImage:img forState:UIControlStateNormal];
    [self.btnGetConfirm setBackgroundImage:imgHl forState:UIControlStateHighlighted];
    [self.btnSendConfirm setBackgroundImage:img forState:UIControlStateNormal];
    [self.btnSendConfirm setBackgroundImage:imgHl forState:UIControlStateHighlighted];
    [self.btnRecvConfirm setBackgroundImage:img forState:UIControlStateNormal];
    [self.btnRecvConfirm setBackgroundImage:imgHl forState:UIControlStateHighlighted];
    
    self.headerViewLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
    
    UIButton *btnsure = [UIButton buttonWithType:UIButtonTypeCustom];
    btnsure.frame = CGRectMake(32, 0, 20, 20);
    btnsure.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"提交按钮"]];
    [btnsure addTarget:self action:@selector(commitClick:) forControlEvents:UIControlEventTouchUpInside];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 62, 22)];
    [view addSubview:btnsure];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.rightBarButtonItem = bar;
    
    self.modifyPriceItemCnt = 0;
    self.modifySuccessCnt = 0;

    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardWillHideNotification object:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.sendBottomView.frame = CGRectMake(0, self.view.bounds.size.height - self.sendBottomView.frame.size.height, self.view.bounds.size.width, self.sendBottomView.frame.size.height);
    self.getBottomView.frame = CGRectMake(0, self.view.bounds.size.height - self.getBottomView.frame.size.height, self.view.bounds.size.width, self.getBottomView.frame.size.height);
    self.recvBottomView.frame = CGRectMake(0, self.view.bounds.size.height - self.recvBottomView.frame.size.height, self.view.bounds.size.width, self.recvBottomView.frame.size.height);
    self.completeBottomView.frame = CGRectMake(0, self.view.bounds.size.height - self.completeBottomView.frame.size.height, self.view.bounds.size.width, self.completeBottomView.frame.size.height);
    
    if (self.tv == nil) {
        self.tv = [[UITableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.headerView.frame.size.height - self.bottomHeight)];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        
        UIView *headViewBack = [self.tvHeader viewWithTag:1];
        headViewBack.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        headViewBack.layer.borderWidth = 1;
        [self.tv setTableHeaderView:self.tvHeader];
        
        UIView *footViewBack = [self.tvBottom viewWithTag:1];
        footViewBack.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        footViewBack.layer.borderWidth = 1;
        [self.tv setTableFooterView:self.tvBottom];
        [self.view addSubview:self.tv];
        [self loadOrderDetailData];
    }
    
    if (self.cover == nil) {
        self.cover = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        self.cover.hidden = YES;
        self.cover.backgroundColor = [UIColor clearColor];
        [self.cover addTarget:self action:@selector(coverOnClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.cover];
    }
    
    [self drawBaseInfo];
}

- (void)drawBaseInfo
{
    if (self.curOrder != nil) {
        self.labName.text = self.curOrder.contactName;
        self.labTel.text = self.curOrder.mobilePhone;
        self.labAddr.text = self.curOrder.orderAddr;
        self.labDes.text = [NSString stringWithFormat:@"备注：%@", self.curOrder.orderDesc];
        if ([self.curOrder.recvType isEqualToString:@"1"]) {
            [self.sendGoods setImage:[UIImage imageNamed:@"RadioDown"]];
            [self.getGoods setImage:[UIImage imageNamed:@"RadioUp"]];
        } else {
            [self.sendGoods setImage:[UIImage imageNamed:@"RadioUp"]];
            [self.getGoods setImage:[UIImage imageNamed:@"RadioDown"]];
        }
    }
    
    UIView *headViewBack = [self.tvHeader viewWithTag:1];
    UILabel *labOrderId = (UILabel *)[headViewBack viewWithTag:2];
    UILabel *labOrderDate = (UILabel *)[headViewBack viewWithTag:3];
    NSString *idStr = [NSString stringWithFormat:@"订单号：%@", self.curOrder.orderId];
    labOrderId.text = idStr;
    labOrderDate.text = self.curOrder.orderDate;
    // resize
    
    UIView *footViewBack = [self.tvBottom viewWithTag:1];
    UILabel *labItemCnt = (UILabel *)[footViewBack viewWithTag:3];
    labItemCnt.text = self.curOrder.qtyOrderSum;
    return;
}

- (void)drawSumMoney
{
    UIView *footViewBack = [self.tvBottom viewWithTag:1];
    self.labSumMoney = (UILabel *)[footViewBack viewWithTag:2];
    float sumMoney = 0;
    for (int i = 0; i < self.itemsTmpArray.count; i++) {
        RYKOrderItem *curItem = [self.itemsTmpArray objectAtIndex:i];
        sumMoney += [curItem.amt floatValue];
    }
    
    self.labSumMoney.text = [NSString stringWithFormat:@"¥ %.2lf", sumMoney];
}

- (void)drawTraceInfo:(RYKOrderTrace *)curTrace;
{
    NSString *deliveryDes;
    if ([self.curOrder.recvType isEqualToString:@"1"]) {
        deliveryDes = [NSString stringWithFormat:@"出货状态：已出货（%@分钟内送达）", curTrace.confirmFlowEstTime];
    } else {
        deliveryDes = @"出货状态：已出货";
    }
    
    if ([self.curOrder.flowCode isEqualToString:@"FINISH"]) {
        // 已完成
        UILabel *labConfirmTime = (UILabel *)[self.completeBottomView viewWithTag:1];
        UILabel *labSendTime = (UILabel *)[self.completeBottomView viewWithTag:2];
        UILabel *labRecvTime = (UILabel *)[self.completeBottomView viewWithTag:3];
        UILabel *labDelivDes = (UILabel *)[self.completeBottomView viewWithTag:6];
        labConfirmTime.text = curTrace.confirmFlowDate;
        labSendTime.text = curTrace.delvPackFlowDate;
        labRecvTime.text = curTrace.finishFlowDate;
        labDelivDes.text = deliveryDes;
    } else {
        // 已出货
        UILabel *labConfirmTime = (UILabel *)[self.recvBottomView viewWithTag:1];
        UILabel *labSendTime = (UILabel *)[self.recvBottomView viewWithTag:2];
        UILabel *labDelivDes = (UILabel *)[self.recvBottomView viewWithTag:6];
        labConfirmTime.text = curTrace.confirmFlowDate;
        labSendTime.text = curTrace.delvPackFlowDate;
        labDelivDes.text = deliveryDes;
    }
    return;
}

- (void)textFieldUp:(NSInteger)height
{
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.sendBottomView.frame;
        frame.origin.y -= height;
        self.sendBottomView.frame = frame;
        self.cover.hidden = NO;
        [self.view bringSubviewToFront:self.sendBottomView];
    }];
    [self.view bringSubviewToFront:self.cover];
    return;
}

- (void)textFieldDown:(NSInteger)height;
{
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.sendBottomView.frame;
        frame.origin.y += height;
        self.sendBottomView.frame = frame;
        self.cover.hidden = YES;
    }];
    return;
}

- (void) keyboardWasShown:(NSNotification *) notif
{
    NSDictionary *info = [notif userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    
    [self textFieldUp:keyboardSize.height];
}

- (void) keyboardWasHidden:(NSNotification *) notif
{
    NSDictionary *info = [notif userInfo];
    
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    [self textFieldDown:keyboardSize.height];
}

#pragma mark -
#pragma mark all button operation
-(IBAction)btnSendConfirmClicked:(UIButton *)btn
{
    if (self.indicator.showing) {
        return;
    }
    if (self.tfSendTime.text.length <= 0) {
        self.indicator.text = @"请输入送达时间";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderConfirmUrl:self.curOrder.orderId withFlowCode:self.curOrder.flowCode withTime:self.tfSendTime.text] delegate:self];
    conn.tag = PROCESS_ORDER_CONFIRM;
    [conn start];
    return;
}

-(IBAction)btnGetConfirmClicked:(UIButton *)btn
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderConfirmUrl:self.curOrder.orderId withFlowCode:self.curOrder.flowCode withTime:@"-1"] delegate:self];
    conn.tag = PROCESS_ORDER_CONFIRM;
    [conn start];
}

-(IBAction)btnRecvConfirmClicked:(UIButton *)btn
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderConfirmUrl:self.curOrder.orderId withFlowCode:@"FINISH" withTime:@"-1"] delegate:self];
    conn.tag = PROCESS_ORDER_RECV_CONFIRM;
    [conn start];
    return;
}

- (void)coverOnClicked
{
    [self.tfSendTime resignFirstResponder];
    return;
}

- (void)commitClick:(UIButton *)btn
{
    NSInteger modifyCnt = 0;
    for (int i = 0; i < self.itemsArray.count; i ++) {
        RYKOrderItem *item = [self.itemsArray objectAtIndex:i];
        RYKOrderItem *tmpItem = [self.itemsTmpArray objectAtIndex:i];
        if ([item.price floatValue] != [tmpItem.price floatValue]) {
            modifyCnt += 1;
        }
    }
    if (modifyCnt <= 0) {
        self.indicator.text = @"未修改价格，无需提交！";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    self.modifyPriceItemCnt = modifyCnt;
    self.modifySuccessCnt = 0;
    
    if (self.indicator.showing) {
        return;
    }
    
     self.indicator.text = @"正在提交中..";
     [self.indicator show];
    
    NSInteger index = 0;
    for (int i = 0; i < self.itemsArray.count; i ++) {
        RYKOrderItem *item = [self.itemsArray objectAtIndex:i];
        RYKOrderItem *tmpItem = [self.itemsTmpArray objectAtIndex:i];
        if ([item.price floatValue] == [tmpItem.price floatValue]) {
            continue;
        }
        
        CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService updateGoodsPrice:self.curOrder.orderId brandId:item.brandId price:tmpItem.price] delegate:self];
        conn.tag = PROCESS_UPDATE_ORDER_GOODSPRICE + index;
        [conn start];
        index += 1;
     }
}


#pragma mark -
#pragma mark all url operation
- (void)loadOrderDetailData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderDetail:self.curOrder.orderId] delegate:self];
    conn.tag = PROCESS_GET_DETAILINFO;
    [conn start];
    return;
}

- (void)loadOrderTraceData
{
    
    if ([self.curOrder.flowCode isEqualToString:@"STORE_CONFIRM"]) {
        return;
    }
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderTrace:self.curOrder.orderId] delegate:self];
    conn.tag = PROCESS_ORDER_TRACE;
    [conn start];
    return;
}

- (void)delivPackGoodsConfirmData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getOrderConfirmUrl:self.curOrder.orderId withFlowCode:@"DELV_PACK" withTime:@"-1"] delegate:self];
    conn.tag = PROCESS_ORDER_DELIVPACK_CONFIRM;
    [conn start];
    return;
}

#pragma mark -
#pragma mark UITableView Delegate * DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.itemsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 96 + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        NSString *cellXibName = @"RYKOrderItemCell";
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSArray* elements = [[NSBundle mainBundle] loadNibNamed:cellXibName owner:nil options:nil];
        
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 5, CGRectGetWidth(tableView.frame) - 10, view.frame.size.height);
        view.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        
        UIView *line = (UIView *)[view viewWithTag:5];
        line.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"xuxian"]];
        
        [cell addSubview:view];
    }
    
    UILabel *labName = (UILabel *)[cell viewWithTag:1];
    CXButton *btnPrice = (CXButton *)[cell viewWithTag:2];
    UILabel *labCnt = (UILabel *)[cell viewWithTag:3];
    UILabel *labSum = (UILabel *)[cell viewWithTag:4];
    UIImageView *ivEdit = (UIImageView *)[cell viewWithTag:6];
    
    
    RYKOrderItem *curItem = [self.itemsTmpArray objectAtIndex:indexPath.row];
    labName.text = curItem.brandName;
    btnPrice.index = indexPath.row;
    [btnPrice  setTitle:[NSString stringWithFormat:@"¥ %@/%@", curItem.price, curItem.brandUnit] forState:UIControlStateNormal];
    [btnPrice  addTarget:self action:@selector(editPrice:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *clearImage = [ZbCommonFunc getImage:CGSizeMake(1, 1) withColor:[UIColor clearColor]];
    [btnPrice setBackgroundImage:clearImage forState:UIControlStateNormal];
    [btnPrice setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    labCnt.text = [NSString stringWithFormat:@"x%@", curItem.qtyOrder];
    labSum.text = [NSString stringWithFormat:@"¥ %@", curItem.amt];
    if ([self.curOrder.flowCode isEqualToString:@"STORE_CONFIRM"]) {
        btnPrice.enabled = YES;
        
        // 显示价格编辑图标，并动态计算图标x坐标
        ivEdit.hidden = NO;
        NSString *priceStr = [btnPrice titleForState:UIControlStateNormal];
        CGSize titleSize = [priceStr sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:btnPrice.frame.size];
        NSInteger editImgX = btnPrice.frame.origin.x + btnPrice.frame.size.width - titleSize.width - 20;
        CGRect ivEditFrame = ivEdit.frame;
        ivEditFrame.origin.x = editImgX;
        ivEdit.frame = ivEditFrame;
        
    } else {
        btnPrice.enabled = NO;
        ivEdit.hidden = YES;
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - editGoodsPrice Event
- (void)editPrice:(CXButton *)btn
{
    self.path = [NSIndexPath indexPathForRow:btn.index inSection:0];
    RYKOrderItem *item = [self.itemsArray objectAtIndex:self.path.row];
    self.labGoodsName = (UILabel *)[self.editGoodsView viewWithTag:10];
    self.labGoodsUnit = (UILabel *)[self.editGoodsView viewWithTag:11];
    self.labGoodsPrice = (UILabel *)[self.editGoodsView viewWithTag:12];
    self.goodsField = (UITextField *)[self.editGoodsView viewWithTag:13];
    self.goodsField.keyboardType = UIKeyboardTypeDecimalPad;
    self.goodsField.delegate = self;
    self.labGoodsName.text = [NSString stringWithFormat:@"商品名称: %@",item.brandName];
    self.labGoodsUnit.text = [NSString stringWithFormat:@"规格: %@",item.brandUnit];
    self.labGoodsPrice.text = [NSString stringWithFormat:@"原价: %@元",item.price];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    self.fullView = [[UIView alloc] initWithFrame:window.bounds];
    self.fullView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:0.5];
    self.editGoodsView.frame = CGRectMake((self.tv.frame.size.width - self.editGoodsView.frame.size.width) / 2, (self.view.frame.size.height - self.editGoodsView.frame.size.height) / 2 - 80, 260, 153);
    self.editGoodsView.backgroundColor = [UIColor whiteColor];
    self.editGoodsView.layer.borderWidth = 1;
    self.editGoodsView.layer.borderColor = [[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] CGColor];
    self.editGoodsView.layer.masksToBounds = YES;
    self.editGoodsView.layer.cornerRadius = 5;
    [self.fullView addSubview:self.editGoodsView];
    [window addSubview:self.fullView];
    [self.goodsField becomeFirstResponder];
}

- (IBAction)btnCancleEditGoods:(id)sender
{
    self.goodsField.text = nil;
    [self.fullView removeFromSuperview];
    return;
}

- (IBAction)btnSureEditGoods:(id)sender
{
    if (self.goodsField.text.length == 0) {
        self.indicator.text = @"输入的现价不能为空";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
        return;
    }
    
    RYKOrderItem *item = [self.itemsTmpArray objectAtIndex:self.path.row];
    float goodsPrice = [self.goodsField.text floatValue];
    item.price = [NSString stringWithFormat:@"%.2lf",goodsPrice];
    item.amt = [NSString stringWithFormat:@"%.2lf", [item.price floatValue] * [item.qtyOrder floatValue]];
    [self drawSumMoney];
    [self.tv reloadData];
    
    [self.fullView removeFromSuperview];
    self.goodsField.text = nil;

    
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (connection.tag >= PROCESS_UPDATE_ORDER_GOODSPRICE) {
        self.modifySuccessCnt = 0;
        self.indicator.text = @"价格修改失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_DETAILINFO){
        RYKOrderItem *orderItems = [[RYKOrderItem alloc] init];
        [orderItems parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            self.indicator.showing = NO;
            
            [self.itemsTmpArray removeAllObjects];
            [self.itemsTmpArray addObjectsFromArray:array];
            
            [self.itemsArray removeAllObjects];
            for (int i = 0; i < self.itemsTmpArray.count; i++) {
                RYKOrderItem *curItem = [self.itemsTmpArray objectAtIndex:i];
                RYKOrderItem *newItem = [[RYKOrderItem alloc] init];
                newItem.brandId = curItem.brandId;
                newItem.brandName = curItem.brandName;
                newItem.brandUnit = curItem.brandUnit;
                newItem.qtyOrder = curItem.qtyOrder;
                newItem.amt = curItem.amt;
                newItem.price = curItem.price;
                [self.itemsArray addObject:newItem];
            }
            [self.tv reloadData];
            [self drawSumMoney];
            [self loadOrderTraceData];
        }];
    } else if (connection.tag == PROCESS_ORDER_CONFIRM) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            self.indicator.text = curMsg.msg;
            if (![curMsg.code isEqualToString:@"1"]) {
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator hide];
            self.indicator.showing = NO;
            // 店铺确认同时已发货
            [self delivPackGoodsConfirmData];
        }];
    } else if (connection.tag == PROCESS_ORDER_DELIVPACK_CONFIRM) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            self.indicator.text = curMsg.msg;
            if (![curMsg.code isEqualToString:@"1"]) {
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0 complete:^(void){
                if (self.delegate && [self.delegate respondsToSelector:@selector(rykOrderDetailViewControllerConfirmOK)]) {
                    [self.delegate rykOrderDetailViewControllerConfirmOK];
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
    } else if (connection.tag == PROCESS_ORDER_RECV_CONFIRM) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg) {
            self.indicator.text = curMsg.msg;
            if (![curMsg.code isEqualToString:@"1"]) {
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0 complete:^(void){
                if (self.delegate && [self.delegate respondsToSelector:@selector(rykOrderDetailViewControllerConfirmOK)]) {
                    [self.delegate rykOrderDetailViewControllerConfirmOK];
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
    } else if (connection.tag == PROCESS_ORDER_TRACE) {
        RYKOrderTrace *trace = [[RYKOrderTrace alloc] init];
        [trace parseData:data complete:^(RYKOrderTrace *curTrace){
            [self.indicator hide];
            [self drawTraceInfo:curTrace];
        }];
    } else if (connection.tag >= PROCESS_UPDATE_ORDER_GOODSPRICE) {
        self.modifySuccessCnt += 1;
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg){
            if (![curMsg.code isEqualToString:@"1"]) {
                self.modifySuccessCnt = 0;
                self.indicator.text = @"价格修改失败";
                [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
                return;
            }
        }];
        if (self.modifySuccessCnt >= self.modifyPriceItemCnt) {
            CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService upOrderPrice:self.curOrder.orderId] delegate:self];
            conn.tag = PROCESS_UPDATE_ORDER_SUM;
            [conn start];
        }
    } else if (connection.tag == PROCESS_UPDATE_ORDER_SUM) {
        self.indicator.text = @"修改成功";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    }
}

@end

