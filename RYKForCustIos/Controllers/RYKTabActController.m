//
//  RYKActivityController.m
//  RYKForCustIos
//
//  Created by 宏超 陈 on 14-6-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKTabActController.h"
#import "CTTableView.h"

@interface RYKTabActController () <CTTableViewDelegate, UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;

- (void)loadActData;

@end

@implementation RYKTabActController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"活动广场" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:116.0/255.0 alpha:1] image:[UIImage imageNamed:@"TabAct"] selectedImage:[UIImage imageNamed:@"TabActHighlighted"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.array = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = @"活动广场";
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
        [self loadActData];
    }
}

#pragma mark -
#pragma mark url requests
- (void)loadActData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载活动...";
    [self.indicator autoHide:CTIndicateStateLoading afterDelay:1.0 complete:^(void) {
        
        UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width - 105) / 2, 85, 105, 113)];
        img.image = [UIImage imageNamed:@"ActNone"];
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 85 + 113)];
        headerView.backgroundColor = [UIColor clearColor];
        [headerView addSubview:img];
        [self.tv setTableHeaderView:headerView];
        
        [self.tv reloadData];
        self.indicator.text = @"暂无活动";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
    }];
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadActData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    return;
}

@end
