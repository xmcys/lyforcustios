//
//  RYKPublicViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-20.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKPublicViewController.h"
#import "CTTableView.h"
#import "RYKWebService.h"
#import "RYKPubMsg.h"
#import "ConsSystemMsg.h"


#define PROCESS_GET_USER_MESSAGE  1
#define PROCESS_GET_SEARCH_MESSAGE 2
#define PROCESS_GET_UNITINFO 3
#define PROCESS_GET_TYPEINFO 4
#define PROCESS_DELETE_MESSAGE 5



@interface RYKPublicViewController () <UITableViewDelegate,UITableViewDataSource,CTTableViewDelegate,UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UIView        *headerview;
@property (nonatomic, strong) IBOutlet UIImageView   *borderview;
@property (nonatomic, strong) CTTableView            *tableview;
@property (nonatomic, strong) NSMutableArray         *msgListArray;
@property (nonatomic, strong) IBOutlet UITextField   *textFiled;
@property (nonatomic, strong) RYKPubMsg              *pubMsg;
@property (nonatomic) NSInteger curDeleteMsgIndex;


- (void)setPubMsg:(RYKPubMsg *)tagsMsgs;
- (IBAction)searchclick:(id)sender;
- (void)deleteUserMessage;



@end

@implementation RYKPublicViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"公告管理";
    }
    return self;
}

- (void)searchclick:(id)sender
{
    [self.textFiled resignFirstResponder];
    [self reloadpubsList];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton *btnadd = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnadd setBackgroundImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    btnadd.frame = CGRectMake(0, 0, 20, 25);
    [btnadd addTarget:self action:@selector(btnaddclick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnright = [[UIBarButtonItem alloc] initWithCustomView:btnadd];
    self.navigationItem.rightBarButtonItem = btnright;
    self.msgListArray = [[NSMutableArray alloc] init];
    self.headerview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TabBg"]];
    self.borderview.layer.cornerRadius = 5;
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_GGGL, CONS_MODULE_GGGL, nil]];
    self.curDeleteMsgIndex = -1;
}

- (void)deleteUserMessage
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    RYKPubMsg *curMsg = [self.msgListArray objectAtIndex:self.curDeleteMsgIndex];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService deleteUsMsgList:curMsg.msgId] delegate:self];
    conn.tag = PROCESS_DELETE_MESSAGE;
    [conn start];
    return;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textFiled resignFirstResponder];
}


- (void)btnaddclick:(UIButton *)btn
{
    RYKAddPublicViewController *controller = [[RYKAddPublicViewController alloc] init];
    controller.delegate = self;
    controller.tag = 2;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tableview == nil) {
        self.tableview = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerview.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height)];
        self.tableview.backgroundColor = [UIColor clearColor];
        self.tableview.pullUpViewEnabled = NO;
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        self.tableview.ctTableViewDelegate = self;
        [self.view addSubview:self.tableview];
        [self loadData];
        
    }
    self.textFiled.delegate = self;
}

- (void)setPubMsg:(RYKPubMsg *)tagMsgs
{
    [self.pubMsg MsgFromotherChanel:tagMsgs];
    return;
}
- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService queryUsMsgList:@""] delegate:self];
    conn.tag = PROCESS_GET_USER_MESSAGE;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    [self.tableview reloadData];
    return;
}
- (void)reloadpubsList
{
    if (self.indicator.showing) {
        return;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService queryUsMsgList:self.textFiled.text] delegate:self];
    conn.tag = PROCESS_GET_SEARCH_MESSAGE;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    [self.tableview reloadData];
}

- (void)RefreshPubDelegate:(BOOL)Refresh
{
    if (Refresh == YES) {
        [self loadData];
    }
    else {
        return;
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self reloadpubsList];
    return YES;
}


#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableview ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tableview ctTableViewDidEndDragging];
}


#pragma mark - CTTableView Delegate * Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.msgListArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        self.curDeleteMsgIndex = indexPath.row;
        [self deleteUserMessage];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKPublicCell" owner:nil options:nil];
        UIView *view = [elements objectAtIndex:0];
        view.frame = CGRectMake(0, 0, tableView.frame.size.width, 90);
        [cell addSubview:view];
    }
    UILabel *titlelab = (UILabel *)[cell  viewWithTag:1];
    UILabel *startdatelab = (UILabel *)[cell  viewWithTag:2];
    UILabel *enddatelab = (UILabel *)[cell viewWithTag:3];
    [titlelab sizeToFit];
    RYKPubMsg *msg = (RYKPubMsg *)[self.msgListArray objectAtIndex:indexPath.row];
    titlelab.text = msg.msgTitle;
    startdatelab.text = msg.publishTime;
    enddatelab.text = msg.validTime;
    return cell;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RYKPubMsg *msg = [self.msgListArray objectAtIndex:indexPath.row];
    RYKAddPublicViewController *controller = [[RYKAddPublicViewController alloc] initWithMsgId:msg.msgId];
    controller.delegate = self;
    controller.tag = 1;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tableview reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_USER_MESSAGE) {
        RYKPubMsg * msg = [[RYKPubMsg alloc] init];
        [msg parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.msgListArray removeAllObjects];
            [self.msgListArray addObjectsFromArray:array];
            [self.tableview reloadData];
        }];
    } else if(connection.tag == PROCESS_GET_SEARCH_MESSAGE) {
            RYKPubMsg * msg = [[RYKPubMsg alloc] init];
            [msg parseData:data complete:^(NSArray *array){
                [self.indicator hide];
                [self.msgListArray removeAllObjects];
                [self.msgListArray addObjectsFromArray:array];
                [self.tableview reloadData];
            }];
    } else if (connection.tag == PROCESS_DELETE_MESSAGE) {
        [self.indicator hide];
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg){
            self.indicator.text = curMsg.msg;
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
            
            if ([curMsg.code isEqualToString:@"1"]) {
                [self.msgListArray removeObjectAtIndex:self.curDeleteMsgIndex];
                // Delete the row from the data source.
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.curDeleteMsgIndex inSection:0];
                [self.tableview deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
