//
//  RYKSaleInfoViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-20.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKSaleInfoViewController.h"
#import "RYKPikcer.h"
#import "CTTableView.h"
#import "RYKWebService.h"
#import "RYKSaleOrder.h"
#import "CTScannerController.h"
#import "RYKRecordersofGoodsViewController.h"
#import "RYKListRecords.h"
#import "CTAlertView.h"

#define PROCESS_LOAD_SALEINFO 1

#define DEF_ORDERDETAIL_STARTINDEX 100


@interface RYKSaleInfoViewController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, RYKPikcerDelegate, CTScannerControllerDelegate,CTAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UILabel *labBottomSumCnt;
@property (strong, nonatomic) IBOutlet UILabel *labBottomSumMoney;
@property (nonatomic, strong) RYKPikcer *datePicker;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *saleOrderArray;
@property (nonatomic) NSInteger totalCnt;
@property (nonatomic) float totalMoney;
@property (nonatomic) NSInteger loadDetailSuccessCnt;

- (void)drawTotalInfo;
- (void)btnBarCodeClicked;
- (void)loadSaleInfoData;
- (void)loadOrderDetailData:(NSString *)orderId withTag:(NSInteger)tag;

@end

@implementation RYKSaleInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"销售信息管理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_XSXX, CONS_MODULE_XSXX, nil]];
    
    UIButton *btnBarCode = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBarCode setBackgroundImage:[UIImage imageNamed:@"BarCode"] forState:UIControlStateNormal];
    btnBarCode.frame = CGRectMake(35, 0, 26, 24);
    [btnBarCode addTarget:self action:@selector(btnBarCodeClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIView *btnview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 62, 24)];
    btnview.backgroundColor = [UIColor clearColor];
    [btnview addSubview:btnBarCode];
    
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithCustomView:btnview];
    
    self.navigationItem.rightBarButtonItem = bar;
    
    self.saleOrderArray = [[NSMutableArray alloc] init];
    self.totalCnt = 0;
    self.totalMoney = 0;
    self.loadDetailSuccessCnt = 0;
    if (!self.curBarCode) {
        self.curBarCode = @"-1";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.datePicker == nil) {
        self.datePicker = [[RYKPikcer alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 1)];
        PickerItem *pickerItem1 = [[PickerItem alloc] init];
        pickerItem1.key = @"-1";
        pickerItem1.value = @"不限时间";
        PickerItem *pickerItem2 = [[PickerItem alloc] init];
        pickerItem2.key = @"30";
        pickerItem2.value = @"近一个月";
        PickerItem *pickerItem3 = [[PickerItem alloc] init];
        pickerItem3.key = @"60";
        pickerItem3.value = @"近二个月";
        PickerItem *pickerItem4 = [[PickerItem alloc] init];
        pickerItem4.key = @"60";
        pickerItem4.value = @"近三个月";
        PickerItem *pickerItem5 = [[PickerItem alloc] init];
        pickerItem5.key = @"180";
        pickerItem5.value = @"近半年";
        NSMutableArray *tempArray = [NSMutableArray arrayWithObjects:pickerItem1, pickerItem2, pickerItem3, pickerItem4, pickerItem5, nil];
        
        [self.datePicker setItemArray:tempArray];
        self.datePicker.delegate = self;
        [self.view addSubview:self.datePicker];
        if ([self.curBarCode isEqualToString:@"-1"]) {
            [self.datePicker setSelectedIndex:1];
        } else {
            [self.datePicker setSelectedIndex:0];
        }
    }
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.datePicker.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.datePicker.frame.size.height- self.bottomView.frame.size.height)];
        
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.ctTableViewDelegate = self;
        self.tv.pullUpViewEnabled = NO;
        self.tv.dataSource = self;
        self.tv.delegate = self;
        [self.view addSubview:self.tv];
    
        CGRect frame = self.bottomView.frame;
        frame.origin.y = self.tv.frame.origin.y + self.tv.frame.size.height;
        self.bottomView.frame = frame;
        [self loadSaleInfoData];
    }
}

- (void)drawTotalInfo
{
    self.labBottomSumCnt.text = [NSString stringWithFormat:@"订单总量：%ld", (long)self.totalCnt];
    self.labBottomSumMoney.text = [NSString stringWithFormat:@"总金额：%.1f", self.totalMoney];
    return;
}

- (void)btnBarCodeClicked
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        CTScannerController * controller = [[CTScannerController alloc] initWithNibName:nil bundle:nil];
        controller.delegate = self;
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        self.indicator.text = @"摄像头不可用";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
    }
    return;
}

#pragma mark -
#pragma mark all url operation
- (void)loadSaleInfoData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    NSString *tagUrl = [RYKWebService getBarCodeSalesOrderListUrl];
    if (([self.curBarCode isEqualToString:@"-1"])) {
        tagUrl = [RYKWebService getSalesOrderListUrl];
    }
    
    PickerItem *daysPicked = [self.datePicker.itemArray objectAtIndex:self.datePicker.selectedIndex];
    NSString *days = daysPicked.key;
    NSString * params = [NSString stringWithFormat:@"<Test><userId>%@</userId><barCode>%@</barCode><date>%@</date></Test>", [RYKWebService sharedUser].userId, self.curBarCode, days];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:tagUrl params:params delegate:self];
    conn.tag = PROCESS_LOAD_SALEINFO;
    [conn start];
    return;
}

- (void)loadOrderDetailData:(NSString *)orderId withTag:(NSInteger)tag
{
    NSString *params = [NSString stringWithFormat:@"<Test><orderId>%@</orderId></Test>", orderId];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getListRecords] params:params delegate:self];
    conn.tag = tag;
    [conn start];
    return;
}
#pragma mark - CTAlertView Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    if (index == 1) {
        NSLog(@"分享成功");
    }
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    self.curBarCode = @"-1";
    [self loadSaleInfoData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.saleOrderArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.curBarCode isEqualToString:@"-1"]) {
        return 200 + 5;
    }
    if (indexPath.row == 0) {
        return 96 + 41 + 5;
    }
    return 96 + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *cellXibName = @"RYKSaleOrderBarCodeCell";
    NSInteger cellY = 5;
    if ([self.curBarCode isEqualToString:@"-1"]) {
        cellXibName = @"RYKSaleOrderCell";
    } else {
        if (indexPath.row == 0) {
            NSArray* brandNameElements = [[NSBundle mainBundle] loadNibNamed:@"RYKSaleOrderBrandNameCell" owner:nil options:nil];
            
            UIView * brandNameview = [brandNameElements objectAtIndex:0];
            [cell addSubview:brandNameview];
            brandNameview.frame = CGRectMake(5, 5, brandNameview.frame.size.width, brandNameview.frame.size.height);
            brandNameview.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1] CGColor];
            brandNameview.layer.borderWidth = 1;
            cellY = brandNameview.frame.origin.y + brandNameview.frame.size.height;
        }
    }
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed:cellXibName owner:nil options:nil];
    
    UIView * view = [elements objectAtIndex:0];
    view.frame = CGRectMake(5, cellY, tableView.frame.size.width - 10, view.frame.size.height);
    view.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1] CGColor];
    view.layer.borderWidth = 1;
    
    [cell addSubview:view];
    
    if ([self.curBarCode isEqualToString:@"-1"]) {
        UILabel *labContactName = (UILabel *)[cell viewWithTag:1];
        UILabel *labDate = (UILabel *)[cell viewWithTag:2];
        UILabel *labBrandName = (UILabel *)[cell viewWithTag:3];
        UILabel *labBrandPrice = (UILabel *)[cell viewWithTag:4];
        UILabel *labBrandCnt = (UILabel *)[cell viewWithTag:5];
        UILabel *labSumMoney = (UILabel *)[cell viewWithTag:6];
        UILabel *labSumCnt = (UILabel *)[cell viewWithTag:7];
        UIImageView *ivLine = (UIImageView *)[cell viewWithTag:8];
        UILabel *labLeftItemCnt = (UILabel *)[cell viewWithTag:9];
        
        RYKSaleOrder *curOrder = [self.saleOrderArray objectAtIndex:indexPath.row];
        labContactName.text = curOrder.contactName;
        labDate.text = curOrder.orderDate;
        labBrandName.text = curOrder.brandName;
        labBrandPrice.text = [NSString stringWithFormat:@"¥ %@/%@", curOrder.price, curOrder.brandUnit];
        labBrandCnt.text = [NSString stringWithFormat:@"x%@", curOrder.brandCnt];
        labSumMoney.text = [NSString stringWithFormat:@"¥ %@", curOrder.amtOrderSum];
        labSumCnt.text = curOrder.qtyOrderSum;
        UIImage *line = [[UIImage imageNamed:@"xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        ivLine.image = line;
        labLeftItemCnt.text = [NSString stringWithFormat:@"显示剩余%@件商品", curOrder.leftBrandCnt];
    } else {
        UILabel *labContactName = (UILabel *)[cell viewWithTag:1];
        UILabel *labDate = (UILabel *)[cell viewWithTag:2];
        UILabel *labSumMoney = (UILabel *)[cell viewWithTag:3];
        UILabel *labSumCnt = (UILabel *)[cell viewWithTag:4];
        
        RYKSaleOrder *curOrder = [self.saleOrderArray objectAtIndex:indexPath.row];
        labContactName.text = curOrder.contactName;
        labDate.text = curOrder.orderDate;
        labSumMoney.text = [NSString stringWithFormat:@"¥ %@", curOrder.amtOrderSum];
        labSumCnt.text = curOrder.qtyOrderSum;
        
        if (indexPath.row == 0) {
            UILabel *labBrandName = (UILabel *)[cell viewWithTag:11];
            UILabel *labBarCode = (UILabel *)[cell viewWithTag:12];
            labBrandName.text = curOrder.brandName;
            labBarCode.text = self.curBarCode;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RYKSaleOrder *curOrder = [self.saleOrderArray objectAtIndex:indexPath.row];
    RYKRecordersofGoodsViewController *controller = [[RYKRecordersofGoodsViewController alloc] initWithOrder:curOrder.orderId];
    controller.orderDate = curOrder.orderDate;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_LOAD_SALEINFO) {
        RYKSaleOrder *saleOrder = [[RYKSaleOrder alloc] init];
        [saleOrder parseData:data complete:^(NSArray *array) {
            [self.saleOrderArray removeAllObjects];
            self.saleOrderArray = [NSMutableArray arrayWithArray:array];
            
            // 统计总额
            self.totalCnt = 0;
            self.totalMoney = 0;
            for (NSInteger i = 0; i < array.count; i++) {
                RYKSaleOrder *curOrder = (RYKSaleOrder *)[self.saleOrderArray objectAtIndex:i];
                self.totalCnt += [curOrder.qtyOrderSum intValue];
                self.totalMoney += [curOrder.amtOrderSum floatValue];
            }
            [self drawTotalInfo];
            [self.tv reloadData];
            if (![self.curBarCode isEqualToString:@"-1"]) {
                [self.indicator hide];
                return;
            }
            
            if (array.count <= 0) {
                [self.indicator hide];
                return;
            }
            self.loadDetailSuccessCnt = 0;
            // 继续加载订单详情
            for (int i = 0; i < array.count; i++) {
                RYKSaleOrder *curSaleOrder = [self.saleOrderArray objectAtIndex:i];
                [self loadOrderDetailData:curSaleOrder.orderId withTag:(DEF_ORDERDETAIL_STARTINDEX + i)];
            }
        }];
    } else if (connection.tag >= DEF_ORDERDETAIL_STARTINDEX) {
        RYKListRecords *orderGoods = [[RYKListRecords alloc] init];
        [orderGoods parseData:data complete:^(NSArray *array){
            
            if (array.count > 0) {
                NSInteger orderIndex = connection.tag - DEF_ORDERDETAIL_STARTINDEX;
                RYKSaleOrder *curOrder = [self.saleOrderArray objectAtIndex:orderIndex];
                RYKListRecords *firstGoods = [array objectAtIndex:0];
                curOrder.brandName = firstGoods.brandName;
                curOrder.brandUnit = firstGoods.brandUnit;
                curOrder.brandCnt = firstGoods.qtyOrder;
                curOrder.price = firstGoods.price;
                curOrder.leftBrandCnt = [NSString stringWithFormat:@"%d", array.count - 1];
            }
            
            self.loadDetailSuccessCnt += 1;
            if (self.loadDetailSuccessCnt >= self.saleOrderArray.count) {
                [self.indicator hide];
                [self.tv reloadData];
            }
        }];
    }
    return;
}
#pragma mark -
#pragma mark RYKPikcer Delegate
- (void)rykPickerDelegate:(RYKPikcer *)picker didSelectRowAtIndex:(int)index
{
    [self loadSaleInfoData];
    return;
}

#pragma mark -
#pragma mark ConsScannerController Delegate
- (void)ctScannerController:(CTScannerController *)controller didReceivedScanResult:(NSString *)result
{
    [controller.navigationController popViewControllerAnimated:NO];
    
    NSError * error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[0-9]*$"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSRange range = [regex rangeOfFirstMatchInString:result options:NSRegularExpressionCaseInsensitive range:NSMakeRange(0, result.length)];
    if (range.location == NSNotFound) {
        self.indicator.text = @"您扫描的并非条码信息";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
    } else {
        self.curBarCode = result;
        [self loadSaleInfoData];
    }
}

- (void)ctScannerControllerDidNavRightItemClicked:(CTScannerController *)controller
{
}

@end
