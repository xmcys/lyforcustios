//
//  RYKAddPublicViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-26.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKAddPublicViewController.h"
#import "CTTableView.h"
#import "CTEditorController.h"
#import "CTDatePicker.h"
#import "RYKPubMsg.h"
#import "RYKWebService.h"
#import "ConsSystemMsg.h"

#define PROCESS_POST_MSGTITLE    1
#define PROCESS_POST_MSGCONT     2
#define PROCESS_GET_MSG 3
#define PROCESS_POST_PUBMSG      4



@interface RYKAddPublicViewController ()<UITableViewDelegate,UITableViewDataSource,CTTableViewDelegate,CTDatePickerDelegate,CTEditorControllerDelegate>
@property (nonatomic, strong) CTTableView      *tableview;
@property (nonatomic, strong) NSMutableArray   *pubarray;
@property (nonatomic, strong) NSArray          *titlearray;
@property (nonatomic, strong) NSMutableArray   *msgarray;
@property (nonatomic, strong) UILabel          *detaillab;
@property (nonatomic, strong) NSString         *params;
@property (nonatomic, strong) NSString         *msgtitle;    //保存消息标题
@property (nonatomic, strong) NSString         *msgpubdate;  //保存发布日期
@property (nonatomic, strong) NSString         *msgvaldate;  //保存结束日期
@property (nonatomic, strong) NSString         *msgmsgcont;  //保存消息内容

@end

@implementation RYKAddPublicViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithMsgId:(NSString *)msgId
{
    if (self = [super init]) {
        self.msgId = msgId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.msgarray = [[NSMutableArray alloc] init];
}

- (void)creatrightbtn
{
    UIButton *btncommit = [UIButton buttonWithType:UIButtonTypeCustom];
    [btncommit setBackgroundImage:[UIImage imageNamed:@"提交按钮"] forState:UIControlStateNormal];
    btncommit.frame = CGRectMake(0, 0, 22, 22);
    [btncommit addTarget:self action:@selector(btncommitclick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnright = [[UIBarButtonItem alloc] initWithCustomView:btncommit];
    self.navigationItem.rightBarButtonItem = btnright;
}

- (void)btncommitclick:(UIButton *)btn
{
    if (self.indicator.showing) {
        return;
    }
    NSString *params =[NSString stringWithFormat:@"<UsMsgList><msgTitle>%@</msgTitle><publishTime>%@</publishTime><validTime>%@</validTime><msgCont>%@</msgCont><publishMan>%@</publishMan></UsMsgList>",self.msgtitle,self.msgpubdate,self.msgvaldate,self.msgmsgcont,[RYKWebService sharedUser].userId];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService saveUsMsgList] params:params delegate:self];
    conn.tag = PROCESS_POST_PUBMSG;
    conn.delegate = self;
    [conn start];
    self.indicator.text = @"正在提交中...";
    [self.indicator show];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tableview == nil) {
        self.tableview = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tableview.backgroundColor = [UIColor clearColor];
        self.tableview.pullUpViewEnabled = NO;
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        self.tableview.ctTableViewDelegate = self;
        self.tableview.pullDownViewEnabled = NO;
        self.tableview.pullUpViewEnabled = NO;
        [self.view addSubview:self.tableview];
    }
    if (self.tag == 1) {
        if (self.indicator.showing) {
            return;
        }
        self.indicator.text = @"努力加载中..";
        [self.indicator show];
        self.title = @"更新公告";
        [self updatePubMsg];
        return;
    }else{
        self.title = @"新增公告";
        [self addPubMsg];
        [self creatrightbtn];

    }
}
- (void)updatePubMsg
{
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getUsMsgListById:self.msgId publishman:[RYKWebService sharedUser].userId] delegate:self];
    conn.tag = PROCESS_GET_MSG;
    [conn start];
}

- (void)addPubMsg
{
}


#pragma mark - CTTableView Delegate * Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"RYKShopInfoCell" owner:nil options:nil];
    UIView *view = [elements objectAtIndex:0];
    view.frame = CGRectMake(0, 0, tableView.frame.size.width, 50);
    [cell addSubview:view];
    self.titlearray = [NSArray arrayWithObjects:@"标题",@"开始日期",@"结束日期",@"内容", nil];
    UILabel *infolab = (UILabel *)[cell viewWithTag:1];
    self.detaillab = (UILabel *)[cell viewWithTag:2];
    infolab.text = [self.titlearray objectAtIndex:indexPath.row];
    if (self.tag == 1) {
        if (self.msgarray.count > 0) {
            RYKPubMsg *info = (RYKPubMsg *)[self.msgarray objectAtIndex:0];
            if (indexPath.row ==  0) {
                self.detaillab.text = info.msgTitle;
            } else if (indexPath.row == 1) {
                if ([info.publishTime isEqualToString:@"null"]) {
                    self.detaillab.text = @"";
                }
                NSString *str = [info.publishTime substringToIndex:11];
                self.detaillab.text = str;
                
            } else if (indexPath.row == 2) {
                if ([info.validTime isEqualToString:@"null"]) {
                    self.detaillab.text = @"";
                }

                NSString *str = [info.validTime substringToIndex:11];
                self.detaillab.text = str;
                
            } else if (indexPath.row == 3) {
                self.detaillab.text = info.msgCont;
            }
        }
    }
    else if (self.tag == 2){
            if (indexPath.row ==  0) {
                self.detaillab.text = @"编辑标题";
            } else if (indexPath.row == 1) {
                
            } else if (indexPath.row == 2) {
                
            } else if (indexPath.row == 3) {
                self.detaillab.text = @"编辑内容";
            }
        }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.tag == 1) {
        if (self.msgarray.count <= 0) {
            return;
        }
        RYKPubMsg *info = [self.msgarray objectAtIndex:0];
        if (indexPath.row == 0) {
            CTEditorController *controller = [[CTEditorController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
            controller.title = [self.titlearray objectAtIndex:indexPath.row];
            controller.delegate = self;
            controller.view.tag = 0;
            [controller setDefaultValue:info.msgTitle];
        }
        else if(indexPath.row == 1){
            CTDatePicker *pick = [[CTDatePicker alloc] initWithDateFormatter:@"YYYY-MM-dd"];
            pick.tag = 1;
            pick.delegate = self;
            NSString *date = [info.publishTime substringToIndex:info.publishTime.length - 11];
            [pick setCurrentDate:date];
            [pick show];
      
        }
        else if(indexPath.row == 2){
            CTDatePicker *pick = [[CTDatePicker alloc] initWithDateFormatter:@"YYYY-MM-dd"];
            pick.tag = 2;
            pick.delegate = self;
            NSString *date = [info.validTime substringToIndex:info.validTime.length - 11];
            [pick setCurrentDate:date];
            [pick show];
        }

        else if(indexPath.row == 3){
            CTEditorController *controller = [[CTEditorController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
            controller.title = [self.titlearray objectAtIndex:indexPath.row];
            controller.editorType = CTEditorTypeTextView;
            controller.delegate = self;
            controller.view.tag = 3;
            [controller setDefaultValue:info.msgCont];
        }
    }
    else if (self.tag == 2) {
        if (indexPath.row == 0) {
            CTEditorController *controller = [[CTEditorController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
            controller.title = [self.titlearray objectAtIndex:indexPath.row];
            controller.delegate = self;
            controller.view.tag = 0;
        }
        else if(indexPath.row == 1){
            CTDatePicker *pick = [[CTDatePicker alloc] initWithDateFormatter:@"YYYY-MM-dd"];
            pick.tag = 1;
            pick.delegate = self;
            [pick show];
            
        }
        else if(indexPath.row == 2){
            CTDatePicker *pick = [[CTDatePicker alloc] initWithDateFormatter:@"YYYY-MM-dd"];
            pick.tag = 2;
            pick.delegate = self;
            [pick show];
        }
        else if(indexPath.row == 3){
            CTEditorController *controller = [[CTEditorController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
            controller.title = [self.titlearray objectAtIndex:indexPath.row];
            controller.editorType = CTEditorTypeTextView;
            controller.delegate = self;
            controller.view.tag = 3;
        }
    }
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    return YES;
}

#pragma mark - CTEditor Delegate
- (void)ctEditorController:(CTEditorController *)controller editDoneWithText:(NSString *)text
{
    NSInteger tag = controller.view.tag;
    if (self.tag == 1) {
        RYKPubMsg *msg =(RYKPubMsg *)[self.msgarray objectAtIndex:0];
        NSString *publishtime = [msg.publishTime substringToIndex:11];
        NSString *validTime = [msg.validTime substringToIndex:11];
        self.params = [NSString stringWithFormat:@"<UsMsgList><msgTitle>%@</msgTitle><publishTime>%@</publishTime><validTime>%@</validTime><msgCont>%@</msgCont></UsMsgList>",msg.msgTitle,publishtime,validTime,msg.msgCont];
        if (tag == 0) {
            self.params = [NSString stringWithFormat:@"<UsMsgList><msgTitle>%@</msgTitle><publishTime>%@</publishTime><validTime>%@</validTime><msgCont>%@</msgCont><msgId>%@</msgId></UsMsgList>",text,publishtime,validTime,msg.msgCont,msg.msgId];
        }
        else if (tag == 3)
        {
            self.params = [NSString stringWithFormat:@"<UsMsgList><msgTitle>%@</msgTitle><publishTime>%@</publishTime><validTime>%@</validTime><msgCont>%@</msgCont><msgId>%@</msgId></UsMsgList>",msg.msgTitle,publishtime,validTime,text,msg.msgId];
        }
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService updateUsMsgList] params:self.params delegate:self];
        conn.tag = PROCESS_POST_MSGTITLE;
        [conn start];
         [self.navigationController popViewControllerAnimated:YES];
    }
    else if (self.tag == 2) {
        UITableViewCell *curCell = [self.tableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tag inSection:0]];
        UILabel *labDetail = (UILabel *)[curCell viewWithTag:2];
        labDetail.text = text;
        [self.navigationController popViewControllerAnimated:YES];
        if (tag == 0) {
            self.msgtitle = text;
        } else if (tag == 3) {
            self.msgmsgcont = text;
        }
    }
}



#pragma mark - CTDatePicker Delegate
- (void)ctDatePicker:(CTDatePicker *)datePicker didSelectDate:(NSString *)date
{
    if (self.tag == 1) {
        RYKPubMsg *msg = (RYKPubMsg *)[self.msgarray objectAtIndex:0];
        NSString *publishtime = [msg.publishTime substringToIndex:11];
        NSString *validTime = [msg.validTime substringToIndex:11];
        NSString *params = [NSString stringWithFormat:@"<UsMsgList><msgTitle>%@</msgTitle><publishTime>%@</publishTime><validTime>%@</validTime><msgCont>%@</msgCont><msgId>%@</msgId></UsMsgList>",msg.msgTitle,publishtime,validTime,msg.msgCont,msg.msgId];
        if (datePicker.tag == 1) {
            params = [NSString stringWithFormat:@"<UsMsgList><msgTitle>%@</msgTitle><publishTime>%@</publishTime><validTime>%@</validTime><msgCont>%@</msgCont><msgId>%@</msgId></UsMsgList>",msg.msgTitle,date,validTime,msg.msgCont,msg.msgId];
        }
        else if (datePicker.tag == 2){
            params = [NSString stringWithFormat:@"<UsMsgList><msgTitle>%@</msgTitle><publishTime>%@</publishTime><validTime>%@</validTime><msgCont>%@</msgCont><msgId>%@</msgId></UsMsgList>",msg.msgTitle,publishtime,date,msg.msgCont,msg.msgId];
        }
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService updateUsMsgList] params:params delegate:self];
        conn.tag = PROCESS_POST_MSGTITLE;
        [conn start];
    }
    else if (self.tag == 2){
        UITableViewCell *curCell = [self.tableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:datePicker.tag inSection:0]];
        UILabel *labDetail = (UILabel *)[curCell viewWithTag:2];
        labDetail.text = date;
        if (datePicker.tag == 1) {
            self.msgpubdate = date;
        }else if (datePicker.tag == 2){
            self.msgvaldate = date;
        }
    }
}

#pragma mark - CTURLConnection Delegate

- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
    [self.tableview reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_POST_MSGTITLE) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.indicator.text = @"修改成功";
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
            [self updatePubMsg];
            [self.tableview reloadData];
            [self.delegate RefreshPubDelegate:YES];
        }
    }
    else if (connection.tag == PROCESS_POST_MSGCONT) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            [self.indicator hide];
            self.indicator.text = @"修改成功";
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
            [self.delegate RefreshPubDelegate:YES];
        }
    }
    else if (connection.tag == PROCESS_GET_MSG){
        RYKPubMsg *info =[[RYKPubMsg alloc] init];
        [info parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.msgarray removeAllObjects];
            [self.msgarray addObjectsFromArray:array];
            [self.tableview reloadData];
        }];
    } else if (connection.tag == PROCESS_POST_PUBMSG){
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.indicator.text = @"新增成功";
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
            [self.delegate RefreshPubDelegate:YES];
        }else {
            self.indicator.text = @"新增失败";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        }
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
