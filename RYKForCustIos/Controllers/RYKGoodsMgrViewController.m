//
//  RYKGoodsMgrViewController.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-6-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKGoodsMgrViewController.h"
#import "RYKPopup.h"
#import "CTTableView.h"
#import "RYKGoods.h"
#import "CTScannerController.h"
#import "RYKAddGoodsViewController.h"
#import "RYKWebService.h"
#import "RYKGoodsType.h"
#import "CTAlertView.h"
#import "ConsSystemMsg.h"
#import "CXButton.h"

#define PROCESS_GET_TYPEDATA 1              // 类型列表加载
#define PROCESS_POST_GOODSLIST 2            // 商品列表加载
#define PROCESS_POST_CHANGESTATE 3          // 商品上下架
#define PROCESS_CHECK_EXISTS 4              // 查找指定条形码
#define PROCESS_MORE_GOODSLIST 5            // 下一页商品列表加载

#define REQUEST_GOODS_CNT_PERTIME 20        // 每次加载条数

#define DEFINE_TYPE_GOODSMGR 1              // 商品管理
#define DEFINE_TYPE_ONSALEGOODSMGR 2        // 促销管理

@implementation RYKGoodsFilterCondition

@end

@interface RYKGoodsMgrViewController ()<CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, RYKPopupDelegate, CTScannerControllerDelegate, UITextFieldDelegate, CTAlertViewDelegate, RYKAddGoodsViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIImageView *inputBg;
@property (nonatomic, strong) IBOutlet UITextField *tfSearch;
@property (nonatomic, strong) RYKPopup * typePop;   // 类型下拉
@property (nonatomic, strong) RYKPopup * pricePop;  // 价格下拉
@property (nonatomic, strong) RYKPopup * statePop;  // 状态下拉
@property (nonatomic, strong) UIView * popBottomLineView;  // 弹出栏底部线条
@property (nonatomic, strong) CTTableView * tv;
@property (nonatomic, strong) NSMutableArray *goodsArray;      // 商品列表
@property (nonatomic, strong) RYKGoodsFilterCondition *lastFilterCondition;
@property (nonatomic, strong) RYKGoods *curOperGoods;

- (IBAction)btnBarCodeClicked:(UIButton *)sender;
- (IBAction)btnSearchClicked:(UIButton *)sender;
- (void)btnAddOnClicked:(UIButton *)btn;
- (void)btnDetailOnClicked:(CXButton *)btn;
- (void)btnDownloadOnClicked:(CXButton *)btn;

- (void)loadGoodsTypeData;
- (void)loadGoodsListData:(RYKGoodsFilterCondition *)fc;
- (void)changeGoodsState;
- (RYKGoodsFilterCondition *)getFilterConditions;
- (void)reloadGoodsList;
- (void)loadMoreGoodsList;

@end

@implementation RYKGoodsMgrViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"商品管理";
        self.curType = DEFINE_TYPE_GOODSMGR;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_SPGL, CONS_MODULE_SPGL, nil]];
    
    self.headerView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.headerView.frame.size.height);
    self.inputBg.image = [[UIImage imageNamed:@"SelectorBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 8)];
    [self.view addSubview:self.headerView];
    
    if (self.curType == DEFINE_TYPE_GOODSMGR) {
        UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAdd setBackgroundImage:[UIImage imageNamed:@"AddSign"] forState:UIControlStateNormal];
        btnAdd.frame = CGRectMake(0, 0, 22, 22);
        [btnAdd addTarget:self action:@selector(btnAddOnClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    }
    
    self.goodsArray = [[NSMutableArray alloc] init];
    self.lastFilterCondition = [[RYKGoodsFilterCondition alloc] init];
    self.curOperGoods = [[RYKGoods alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.typePop == nil) {
        
        NSInteger headerViewHeight = self.headerView.frame.size.height;
        self.typePop = [[RYKPopup alloc] initWithFrame:CGRectMake(0, headerViewHeight, self.view.bounds.size.width / 3, 35)];
        self.typePop.delegate = self;
        [self.view addSubview:self.typePop];
        
        self.pricePop = [[RYKPopup alloc] initWithFrame:CGRectMake(self.view.bounds.size.width / 3, headerViewHeight, self.view.bounds.size.width / 3, 35)];
        self.pricePop.delegate = self;
        [self.view addSubview:self.pricePop];
        
        self.statePop = [[RYKPopup alloc] initWithFrame:CGRectMake(self.view.bounds.size.width / 3 * 2, headerViewHeight, self.view.bounds.size.width / 3, 35)];
        self.statePop.delegate = self;
        [self.view addSubview:self.statePop];
        
        UIImageView * sep1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.typePop.frame.size.width - 1, 5, 2, 25)];
        sep1.image = [[UIImage imageNamed:@"分割线"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self.typePop addSubview:sep1];

        UIImageView * sep2 = [[UIImageView alloc] initWithFrame:CGRectMake(self.pricePop.frame.size.width - 1, 5, 2, 25)];
        sep2.image = [[UIImage imageNamed:@"分割线"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self.pricePop addSubview:sep2];
        
        self.popBottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.typePop.frame.origin.y + self.typePop.frame.size.height, self.view.bounds.size.width, 1)];
        [self.popBottomLineView setBackgroundColor:[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1]];
        [self.view addSubview:self.popBottomLineView];
        
        PopupItem *price01 = [[PopupItem alloc] init];
        price01.key = @"0";
        price01.value = @"不限价格";
        PopupItem *price02 = [[PopupItem alloc] init];
        price02.key = @"1";
        price02.value = @"10以下";
        PopupItem *price03 = [[PopupItem alloc] init];
        price03.key = @"2";
        price03.value = @"10-30";
        PopupItem *price04 = [[PopupItem alloc] init];
        price04.key = @"3";
        price04.value = @"30-50";
        PopupItem *price05 = [[PopupItem alloc] init];
        price05.key = @"4";
        price05.value = @"50-100";
        PopupItem *price06 = [[PopupItem alloc] init];
        price06.key = @"5";
        price06.value = @"100以上";
        NSMutableArray *priceArray = [NSMutableArray arrayWithObjects:price01, price02, price03, price04, price05, price06, nil];
        [self.pricePop setItemArray:priceArray];
        
        PopupItem *state01 = [[PopupItem alloc] init];
        state01.key = @"-1";
        state01.value = @"不限状态";
        PopupItem *state02 = [[PopupItem alloc] init];
        state02.key = @"1";
        state02.value = @"已上架";
        PopupItem *state03 = [[PopupItem alloc] init];
        state03.key = @"0";
        state03.value = @"已下架";
        NSMutableArray *stateArray = [NSMutableArray arrayWithObjects:state01, state02, state03, nil];
        [self.statePop setItemArray:stateArray];
        [self.statePop setSelectedIndex:0];
    }
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height + self.typePop.frame.size.height + 1, self.view.bounds.size.width, self.view.bounds.size.height - self.headerView.frame.size.height - self.typePop.frame.size.height)];
        
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.ctTableViewDelegate = self;
//        self.tv.pullUpViewEnabled = YES;
//        self.tv.pullDownViewEnabled = YES;
        self.tv.dataSource = self;
        self.tv.delegate = self;
        
        [self.view addSubview:self.tv];
        [self loadGoodsTypeData];
    }
    
    if (self.curType == DEFINE_TYPE_ONSALEGOODSMGR) {
        self.title = @"促销管理";
        self.headerView.hidden = YES;
        self.typePop.hidden = YES;
        self.pricePop.hidden = YES;
        self.statePop.hidden = YES;
        self.popBottomLineView.hidden = YES;
        self.tv.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    }
}

#pragma mark -
#pragma mark all button event
- (IBAction)btnBarCodeClicked:(UIButton *)sender
{
    [self.tfSearch resignFirstResponder];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        CTScannerController * controller = [[CTScannerController alloc] initWithNibName:nil bundle:nil];
        controller.delegate = self;
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        self.indicator.text = @"摄像头不可用";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1];
    }
    return;
}

- (IBAction)btnSearchClicked:(UIButton *)sender
{
    [self.tfSearch resignFirstResponder];
    [self reloadGoodsList];
    return;
}

- (void)btnAddOnClicked:(UIButton *)btn
{
    // 添加无条码商品
    RYKGoods *newGoods = [[RYKGoods alloc] init];
    
    RYKAddGoodsViewController *controller = [[RYKAddGoodsViewController alloc] initWithNibName:@"RYKAddGoodsController" bundle:nil];
    [controller setGoodsInfo:newGoods];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

- (void)btnDetailOnClicked:(CXButton *)btn
{
    self.curOperGoods = (RYKGoods *)[self.goodsArray objectAtIndex:btn.index];
    RYKAddGoodsViewController *controller = [[RYKAddGoodsViewController alloc] initWithNibName:@"RYKAddGoodsController" bundle:nil];
    [controller setGoodsInfo:self.curOperGoods];
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

- (void)btnDownloadOnClicked:(CXButton *)btn
{
    self.curOperGoods = [self.goodsArray objectAtIndex:btn.index];
    [self changeGoodsState];
    return;
}

#pragma mark -
#pragma mark all url operation
- (void)loadGoodsTypeData
{
    if (self.indicator.showing) {
        return;
    }
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getGoodsTypeUrl] delegate:self];
    conn.tag = PROCESS_GET_TYPEDATA;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

- (void)loadGoodsListData:(RYKGoodsFilterCondition *)fc
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    self.lastFilterCondition = fc;
    NSString * params = [NSString stringWithFormat:@"<Test><userId>%@</userId><brandName>%@</brandName><brandTypeName>%@</brandTypeName><minRetailPrice>%@</minRetailPrice><maxRetailPrice>%@</maxRetailPrice><pageStart>%@</pageStart><pageEnd>%@</pageEnd><usedFlag>%@</usedFlag><discountFlag>%@</discountFlag></Test>", [RYKWebService sharedUser].userId, fc.searchName, fc.typeName, fc.minPrice, fc.maxPrice, fc.pageStart, fc.pageEnd, fc.stateName, fc.discountFlag];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getGoodsListUrl] params:params delegate:self];
    conn.tag = fc.tag;
    [conn start];
    return;
}

- (void)changeGoodsState
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在提交中..";
    [self.indicator show];
    
    NSString *tagStateName = @"0";
    if ([self.curOperGoods.stateName isEqualToString:@"0"]) {
        tagStateName = @"1";
    }
    
    NSString * params = [NSString stringWithFormat:@"<Test><userId>%@</userId><brandId>%@</brandId><usedFlag>%@</usedFlag></Test>", [RYKWebService sharedUser].userId, self.curOperGoods.goodsId, tagStateName];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService changeGoodsStateUrl] params:params delegate:self];
    conn.tag = PROCESS_POST_CHANGESTATE;
    [conn start];
    return;
}

- (RYKGoodsFilterCondition *)getFilterConditions
{
    RYKGoodsFilterCondition *filterCondition = [[RYKGoodsFilterCondition alloc] init];
    filterCondition.searchName = self.tfSearch.text;
    if (filterCondition.searchName.length == 0) {
        filterCondition.searchName = @"-1";
    }
    
    filterCondition.typeName = ((PopupItem *)[self.typePop.itemArray objectAtIndex:self.typePop.selectedIndex]).value;
    
    filterCondition.minPrice = @"0";
    filterCondition.maxPrice = @"100000";
    NSString *priceValue = ((PopupItem *)[self.pricePop.itemArray objectAtIndex:self.pricePop.selectedIndex]).key;
    if ([priceValue isEqualToString:@"1"]) {
        filterCondition.minPrice = @"0";
        filterCondition.maxPrice = @"10";
    } else if ([priceValue isEqualToString:@"2"]) {
        filterCondition.minPrice = @"10";
        filterCondition.maxPrice = @"30";
    } else if ([priceValue isEqualToString:@"3"]) {
        filterCondition.minPrice = @"30";
        filterCondition.maxPrice = @"50";
    } else if ([priceValue isEqualToString:@"4"]) {
        filterCondition.minPrice = @"50";
        filterCondition.maxPrice = @"100";
    } else if ([priceValue isEqualToString:@"5"]) {
        filterCondition.minPrice = @"100";
        filterCondition.maxPrice = @"100000";
    }
    
    // 上下架状态
    NSString *stateName = ((PopupItem *)[self.statePop.itemArray objectAtIndex:self.statePop.selectedIndex]).key;
    if (self.curType == DEFINE_TYPE_ONSALEGOODSMGR) {
        stateName = @"-1";
    }
    filterCondition.stateName = stateName;
    
    // 是否促销状态
    NSString *discountFlag = @"-1";
    if (self.curType == DEFINE_TYPE_ONSALEGOODSMGR) {
        discountFlag = @"1";
    }
    filterCondition.discountFlag = discountFlag;
    return filterCondition;
}

- (void)reloadGoodsList
{
    RYKGoodsFilterCondition *fc = [self getFilterConditions];
    fc.pageStart = @"0";
    fc.pageEnd = [NSString stringWithFormat:@"%d", REQUEST_GOODS_CNT_PERTIME];
    fc.tag = PROCESS_POST_GOODSLIST;
    [self loadGoodsListData:fc];
    return;
}

- (void)loadMoreGoodsList
{
    RYKGoodsFilterCondition *fc = [self getFilterConditions];
    fc.pageStart = [NSString stringWithFormat:@"%lu", (unsigned long)self.goodsArray.count];
    fc.pageEnd = [NSString stringWithFormat:@"%u", self.goodsArray.count + REQUEST_GOODS_CNT_PERTIME];
    fc.tag = PROCESS_MORE_GOODSLIST;
    [self loadGoodsListData:fc];
    return;
}

#pragma mark -
#pragma mark RYKPopup Delegate
- (void)popUp:(RYKPopup *)rykPopup didSelecteInIndex:(NSInteger)index
{
    [self.tfSearch resignFirstResponder];
    [self reloadGoodsList];
    return;
}

- (void)popUp:(RYKPopup *)rykPopup popStateChanged:(BOOL)isShowing
{
    [self.tfSearch resignFirstResponder];
    if (isShowing) {
        if (rykPopup == self.typePop) {
            [self.pricePop hidePopup];
            [self.statePop hidePopup];
        } else if (rykPopup == self.pricePop){
            [self.typePop hidePopup];
            [self.statePop hidePopup];
        } else {
            [self.typePop hidePopup];
            [self.pricePop hidePopup];
        }
    }
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
    [self.tfSearch resignFirstResponder];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self reloadGoodsList];
    return YES;
}

- (BOOL)ctTableViewDidPullUpToLoad:(CTTableView *)ctTableView
{
    [self loadMoreGoodsList];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.goodsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"RYKGoodsCell" owner:nil options:nil];
    
    UIView * view = [elements objectAtIndex:0];
    view.frame = CGRectMake(5, 5, tableView.frame.size.width - 10, 100);
    view.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1] CGColor];
    view.layer.borderWidth = 1;
    
    [cell addSubview:view];
    
    UIImageView *ivGoodsType = (UIImageView *)[cell viewWithTag:1];
    UILabel *labGoodsType = (UILabel *)[cell viewWithTag:2];
    CXButton *btnDetail = (CXButton *)[cell viewWithTag:3];
    CXButton *btnDownload = (CXButton *)[cell viewWithTag:4];
    UILabel *labName = (UILabel *)[cell viewWithTag:5];
    UILabel *labMoney = (UILabel *)[cell viewWithTag:6];
    
    NSString *goodsTypeImgName;
    RYKGoods *curGoods = (RYKGoods *)[self.goodsArray objectAtIndex:indexPath.row];
    if ([curGoods.goodsTypeId isEqualToString:@"1000"]) {
        goodsTypeImgName = @"GoodsType02";
    } else if ([curGoods.goodsTypeId isEqualToString:@"1203"]) {
        goodsTypeImgName = @"GoodsType03";
    } else if ([curGoods.goodsTypeId isEqualToString:@"14"]) {
        goodsTypeImgName = @"GoodsType04";
    } else if ([curGoods.goodsTypeId isEqualToString:@"11"]) {
        goodsTypeImgName = @"GoodsType01";
    } else if ([curGoods.goodsTypeId isEqualToString:@"1202"]) {
        goodsTypeImgName = @"GoodsType05";
    } else if ([curGoods.goodsTypeId isEqualToString:@"1901"]) {
        goodsTypeImgName = @"GoodsType06";
    } else if ([curGoods.goodsTypeId isEqualToString:@"1201"]) {
        goodsTypeImgName = @"GoodsType08";
    } else if ([curGoods.goodsTypeId isEqualToString:@"1412"]) {
        goodsTypeImgName = @"GoodsType09";
    } else {
        goodsTypeImgName = @"GoodsType07";
    }
    ivGoodsType.image = [UIImage imageNamed:goodsTypeImgName];
    labGoodsType.text = curGoods.goodsTypeName;
    btnDetail.index = indexPath.row;
    [btnDetail addTarget:self action:@selector(btnDetailOnClicked:) forControlEvents:UIControlEventTouchUpInside];
    NSString *stateImgName = @"GoodsDownload";
    if ([curGoods.stateName isEqualToString:@"0"]) {
        stateImgName = @"GoodsUpload";
    }
    btnDownload.index = indexPath.row;
    [btnDownload setImage:[UIImage imageNamed:stateImgName] forState:UIControlStateNormal];
    [btnDownload setImage:[UIImage imageNamed:stateImgName] forState:UIControlStateHighlighted];
    [btnDownload addTarget:self action:@selector(btnDownloadOnClicked:) forControlEvents:UIControlEventTouchUpInside];
    labName.text = curGoods.goodsName;
    labMoney.text = [NSString stringWithFormat:@"¥ %@/%@", curGoods.goodsPrice, curGoods.goodsUnit];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_TYPEDATA) {
//        [self.indicator hide];
        self.indicator.showing = NO;
        RYKGoodsType *goodsType = [[RYKGoodsType alloc] init];
        [goodsType parseData:data complete:^(NSArray *array){
            NSMutableArray * temp = [[NSMutableArray alloc] init];
            PopupItem *defaultItem = [[PopupItem alloc] init];
            defaultItem.key = @"-1";
            defaultItem.value = @"不限类型";
            [temp addObject:defaultItem];
            for (int i = 0; i < array.count; i++) {
                PopupItem * item = [[PopupItem alloc] init];
                RYKGoodsType * b = [array objectAtIndex:i];
                item.key = b.typeCode;
                item.value = b.typeName;
                [temp addObject:item];
            }
            [self.typePop setItemArray:temp];
            [self.typePop setSelectedIndex:0];
            [self reloadGoodsList];
        }];
    } else if (connection.tag == PROCESS_POST_GOODSLIST) {
        [self.indicator hide];
        RYKGoods *goods = [[RYKGoods alloc] init];
        [goods parseData:data complete:^(NSArray *array){
            [self.goodsArray removeAllObjects];
            [self.goodsArray addObjectsFromArray:array];
            [self.tv reloadData];
            [self.tv setContentOffset:CGPointMake(0, 0) animated:YES];
        }];
    } else if (connection.tag == PROCESS_MORE_GOODSLIST) {
        [self.indicator hide];
        RYKGoods *goods = [[RYKGoods alloc] init];
        [goods parseData:data complete:^(NSArray *array){
            [self.goodsArray addObjectsFromArray:array];
            [self.tv reloadData];
        }];
    } else if (connection.tag == PROCESS_CHECK_EXISTS) {
        [self.indicator hide];
        RYKGoods *goods = [[RYKGoods alloc] init];
        [goods parseData:data complete:^(NSArray *array){
            if (array.count > 0) {
                // 有查找到指定条形码
                [self.goodsArray removeAllObjects];
                [self.goodsArray addObjectsFromArray:array];
                [self.tv reloadData];
            } else {
                // 未找到则询问是否新增此商品
                NSString *msg = [NSString stringWithFormat:@"是否马上添加条形码为%@的商品？", self.lastFilterCondition.searchName];
                CTAlertView *alert = [[CTAlertView alloc] initWithTitle:@"未找到指定商品" message:msg delegate:self cancelButtonTitle:@"稍后再说" confirmButtonTitle:@"立即添加"];
                [alert show];
            }
        }];
    } else if (connection.tag == PROCESS_POST_CHANGESTATE) {
        [self.indicator hide];
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *curMsg){
            self.indicator.text = curMsg.msg;
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
            
            if ([curMsg.code isEqualToString:@"1"]) {
                // 上下架操作成功
                NSInteger index = [self.goodsArray indexOfObject:self.curOperGoods];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                
                if ([self.lastFilterCondition.stateName isEqualToString:@"-1"]) {
                    // 更新商品上架状态
                    if ([self.curOperGoods.stateName isEqualToString:@"0"]) {
                        self.curOperGoods.stateName = @"1";
                    } else {
                        self.curOperGoods.stateName = @"0";
                    }
                    // 当前在不限状态中，直接刷新视图
                    [self.tv reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
                    return;
                }
                
                // 删除此商品视图
                [self.goodsArray removeObject:self.curOperGoods];
                [self.tv deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationRight];
                return;
            }
        }];
    }
    return;
}

#pragma mark -
#pragma mark ConsScannerController Delegate
- (void)ctScannerController:(CTScannerController *)controller didReceivedScanResult:(NSString *)result
{
    [controller.navigationController popViewControllerAnimated:NO];
    
    NSError * error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[0-9]*$"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSRange range = [regex rangeOfFirstMatchInString:result options:NSRegularExpressionCaseInsensitive range:NSMakeRange(0, result.length)];
    if (range.location == NSNotFound) {
        self.indicator.text = @"您扫描的并非条码信息";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
    } else {
        RYKGoodsFilterCondition *fc = [[RYKGoodsFilterCondition alloc] init];
        fc.searchName = result;
        fc.typeName = ((PopupItem *)[self.typePop.itemArray objectAtIndex:0]).value;
        fc.minPrice = @"-1";
        fc.maxPrice = @"-1";
        fc.pageStart = @"0";
        fc.pageEnd = [NSString stringWithFormat:@"%d", REQUEST_GOODS_CNT_PERTIME];
        fc.stateName = @"-1";
        fc.discountFlag = @"-1";
        fc.tag = PROCESS_CHECK_EXISTS;
        [self loadGoodsListData:fc];
    }
}

- (void)ctScannerControllerDidNavRightItemClicked:(CTScannerController *)controller
{
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self reloadGoodsList];
    return YES;
}

#pragma mark -
#pragma mark CTAlertView Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    if (index == 0) {
        return;
    }
    
    RYKGoods *newGoods = [[RYKGoods alloc] init];
    newGoods.goodsCode = self.lastFilterCondition.searchName;
    
    RYKAddGoodsViewController *controller = [[RYKAddGoodsViewController alloc] initWithNibName:@"RYKAddGoodsController" bundle:nil];
    [controller setGoodsInfo:newGoods];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

#pragma mark -
#pragma mark rykAddGoodsViewController Delegate
- (void)rykAddGoodsViewControllerModifyOK:(RYKGoods *)goods
{
    if (self.curType == DEFINE_TYPE_ONSALEGOODSMGR) {
        [self reloadGoodsList];
        return;
    }
    
    NSInteger index = [self.goodsArray indexOfObject:self.curOperGoods];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.curOperGoods infoFromOtherGoods:goods];
    [self.tv reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
    return;
}

@end
