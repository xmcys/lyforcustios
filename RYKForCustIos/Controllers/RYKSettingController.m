//
//  ConsSettingController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-28.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKSettingController.h"
#import "RYKWebService.h"
#import "RYKPwdModifyController.h"
#import "RYKCustSignInController.h"
#import "RYKShopViewController.h"

@interface RYKSettingController () <UIActionSheetDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView * container;
@property (nonatomic, strong) IBOutlet UIButton * btnMusic;
@property (nonatomic, strong) IBOutlet UIButton * btnLogout;

- (IBAction)useMusic:(UIButton *)btn;
- (IBAction)accountInfo:(UIButton *)btn;
- (IBAction)pwdModify:(UIButton *)btn;
- (IBAction)showHelp:(UIButton *)btn;
- (IBAction)loginOrLogout:(UIButton *)sender;

@end

@implementation RYKSettingController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"设置";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:self.container];
    
    [self.btnLogout setBackgroundImage:[[UIImage imageNamed:@"BtnGreenNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 3, 10, 3)] forState:UIControlStateNormal];
    [self.btnLogout setBackgroundImage:[[UIImage imageNamed:@"BtnGreenHighLight"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 3, 10, 3)] forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.container.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    if (self.btnLogout.frame.origin.y + self.btnLogout.frame.size.height > self.view.frame.size.height) {
        self.container.contentSize = CGSizeMake(self.container.frame.size.width, self.btnLogout.frame.origin.y + self.btnLogout.frame.size.height + 10);
    } else {
        self.container.contentSize = CGSizeMake(self.container.frame.size.width, self.view.bounds.size.height + 1);
    }
}

- (IBAction)useMusic:(UIButton *)btn
{
    return;
}

- (IBAction)accountInfo:(UIButton *)btn
{
    RYKShopViewController *controller = [[RYKShopViewController alloc] initWithNibName:nil bundle:nil];    
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

- (IBAction)pwdModify:(UIButton *)btn
{
    RYKPwdModifyController * controller = [[RYKPwdModifyController alloc] initWithNibName:@"RYKPwdModifyController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)showHelp:(UIButton *)btn
{
}

- (void)loginOrLogout:(UIButton *)sender
{
    UIActionSheet * action = [[UIActionSheet alloc] initWithTitle:@"确定注销当前账号？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"确定" otherButtonTitles: nil];
    action.tag = 1;
    [action showInView:self.view];
}

#pragma mark -
#pragma mark UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1 && buttonIndex == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_LOGOUT object:nil];
    }
}

@end
