//
//  RYKAddPublicViewController.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-26.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

@protocol RykRefreshPubDelegate <NSObject>

@optional

- (void)RefreshPubDelegate:(BOOL)Refresh;

@end

#import "CTViewController.h"
#import "RYKPubMsg.h"

@interface RYKAddPublicViewController : CTViewController

@property (nonatomic) NSInteger tag;
@property (nonatomic, strong) NSString *msgId;
@property (nonatomic, weak) id<RykRefreshPubDelegate> delegate;

- (id)initWithMsgId:(NSString *)msgId;

@end
