//
//  RYKWmpViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-11-18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKWmpViewController.h"
#import "RYKWebService.h"
#import "RYKPikcer.h"
#import "RYKWmpData.h"
#import "CTImageView.h"
#import "RYKWmpImg.h"
#import "ConsSystemMsg.h"
#import "VPImageCropperViewController.h"
#import "ASIFormDataRequest.h"


#define PROCESS_GET_DATA 1
#define PROCESS_GET_PICURL 2
#define PROCESS_POST_DEFAU  3
#define PROCESS_POST_MODIFY 4

@interface RYKWmpViewController ()<CTURLConnectionDelegate,RYKPikcerDelegate,CTImageViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,VPImageCropperDelegate,ASIHTTPRequestDelegate>

@property (nonatomic, strong) RYKPikcer *wmpPicker;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) IBOutlet UIView *headView;
@property (nonatomic, strong) IBOutlet CTImageView *img;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) PickerItem *currItem;
@property (nonatomic, strong) NSString *uploadImgName;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic) BOOL isPickImg;
@property (nonatomic, strong) NSString *flag;

@end

@implementation RYKWmpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"微信菜单管理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.isPickImg = NO;
    self.uploadImgName = @"";
    self.array = [[NSMutableArray alloc] init];
    self.dataArray = [[NSMutableArray alloc] init];
    UIButton *btnComit = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnComit setBackgroundImage:[UIImage imageNamed:@"提交按钮"] forState:UIControlStateNormal];
    btnComit.frame = CGRectMake(0, 0, 22, 22);
    [btnComit addTarget:self action:@selector(btnComitClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnComit];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RYK_NOTIFICATION_MODULE object:[NSArray arrayWithObjects:CONS_MODULE_WXCD, CONS_MODULE_WXCD, nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.wmpPicker == nil) {
        self.wmpPicker = [[RYKPikcer alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        [self.view addSubview:self.wmpPicker];
        self.wmpPicker.delegate = self;
        self.headView.frame = CGRectMake(0, self.wmpPicker.frame.size.height + self.wmpPicker.frame.origin.y + 10, self.headView.frame.size.width, self.headView.frame.size.height);
        [self.view addSubview:self.headView];
        
        self.img.userInteractionEnabled = YES;
        self.img.delegate = self;
        self.img.image = [UIImage imageNamed:@"ShareAddImg"] ;
        self.img.contentMode = UIViewContentModeScaleToFill;
        
        UIButton *pbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        pbtn.frame = CGRectMake(75, self.view.frame.size.height - 80, self.view.bounds.size.width -  75 * 2, 30);
        pbtn.backgroundColor = XCOLOR(4, 168, 117, 1);
        [pbtn setTitle:@"恢复默认" forState:UIControlStateNormal];
        pbtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [pbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        pbtn.layer.cornerRadius = 6;
        pbtn.layer.masksToBounds = YES;
        [pbtn addTarget:self action:@selector(defaultClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pbtn];
        [self loadData];
    }
}

- (void)btnComitClicked:(UIButton *)btn
{
    if (self.isPickImg == YES) {
        if (self.indicator.showing) {
            return;
        }
        self.indicator.text = @"正在上传图片..";
        [self.indicator show];
        
        NSString * path = [RYKWebService postImageUrl];
        NSURL* url = [NSURL URLWithString:path];
        ASIFormDataRequest* request = [ASIFormDataRequest requestWithURL:url];
        request.showAccurateProgress = YES;
        request.shouldContinueWhenAppEntersBackground = YES;
        request.delegate = self;
        [request setRequestMethod:@"POST"];
        [request setPostFormat:ASIMultipartFormDataPostFormat];
        
        NSDate *nowDate = [NSDate date];
        NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[nowDate timeIntervalSince1970]];
        self.imageName = [NSString stringWithFormat:@"iOS%@.jpg", timeSp];
        self.uploadImgName = [[@"uniService/Resource/custMobile/" stringByAppendingPathComponent:[RYKWebService sharedUser].userId] stringByAppendingPathComponent:self.imageName];
        
        NSData* imgData = UIImageJPEGRepresentation(self.img.image, 0.85);
        [request addData:imgData withFileName:self.imageName andContentType:@"image/jpeg" forKey:@"pic[]"];
        [request start];
        
    } else {
        
        if (self.indicator.showing) {
            return;
        }
        self.indicator.text = @"没有修改,无需提交";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
    }
}

- (void)defaultClick:(UIButton *)btn
{
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><menuCode>%@</menuCode></Test>",[RYKWebService sharedUser].userId,self.currItem.key];
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getRestoreDefault] params:params delegate:self];
    conn.tag = PROCESS_POST_DEFAU;
    [conn start];
}

- (void)loadData
{
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getDictData] delegate:self];
    conn.tag = PROCESS_GET_DATA;
    [conn start];
}

- (void)drawData
{
    for (int i = 0; i < self.array.count; i ++) {
        RYKWmpData *item = (RYKWmpData *)[self.array objectAtIndex:i];
        PickerItem *pickerItem = [[PickerItem alloc] init];
        pickerItem.key = item.dictCode;
        pickerItem.value = item.dictName;
        [self.dataArray addObject:pickerItem];
    }
    [self.wmpPicker setItemArray:self.dataArray];
    if (self.dataArray.count == 0) {
        return;
    }
    self.wmpPicker.selectedIndex = 0;
    self.wmpPicker.delegate = self;
}

- (void)insertPicData
{
    NSString *params = [NSString stringWithFormat:@"<Test><userId>%@</userId><menuCode>%@</menuCode><menuName>%@</menuName><picUrl>%@</picUrl><flag>%@</flag></Test>",[RYKWebService sharedUser].userId,self.currItem.key,self.currItem.value,self.imageName,self.flag];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService getSavePic] params:params delegate:self];
    conn.tag = PROCESS_POST_MODIFY;
    [conn start];
}
#pragma mark - rykPickerDelegate
- (void)rykPickerDelegate:(RYKPikcer *)picker didSelectRowAtIndex:(int)index
{
    self.currItem = [self.dataArray objectAtIndex:index];
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[RYKWebService getDisplayImages:[RYKWebService sharedUser].userId menuCode:self.currItem.key]delegate:self];
    conn.tag = PROCESS_GET_PICURL;
    [conn start];
}
#pragma mark - CTImageView Delegate
- (void)ctImageViewDidClicked:(CTImageView *)ctImageView
{
    UIActionSheet *actSheet;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从手机相册选择", @"拍照", nil];
    } else {
        actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从手机相册选择", nil];
    }
    [actSheet showInView:self.view];
    return;
}

- (void)ctImageViewLoadDone:(CTImageView *)ctImageView withResult:(CTImageViewState)state
{
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    if (buttonIndex == 0) {
        // 相册选择
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    } else {
        // 拍照
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
}
#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        // 重新生成原图，去除图片的横、竖等属性
        UIGraphicsBeginImageContext(portraitImg.size);
        [portraitImg drawInRect:CGRectMake(0, 0, portraitImg.size.width, portraitImg.size.height)];
        UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // 裁剪
        VPImageCropperViewController *imgEditorVC = [[VPImageCropperViewController alloc] initWithImage:reSizeImage cropFrame:CGRectMake(0, (self.view.frame.size.height - self.view.frame.size.width / 1.8) / 2, self.view.frame.size.width, self.view.frame.size.width / 1.8) limitScaleRatio:3.0];
        imgEditorVC.delegate = self;
        [self presentViewController:imgEditorVC animated:YES completion:nil];
    }];
}

#pragma mark - VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    [cropperViewController dismissViewControllerAnimated:YES completion:nil];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    // 缩小图片
    UIGraphicsBeginImageContext(CGSizeMake(360, 200));
    [editedImage drawInRect:CGRectMake(0, 0, 360, 200)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.img.url = nil;
    self.img.image = reSizeImage;
    self.isPickImg = YES;
    return;
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController
{
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    return;
}
#pragma mark - CTUrlConnention Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_DATA) {
        RYKWmpData *item = [[RYKWmpData alloc] init];
        [item parseData:data complete:^(NSMutableArray *array) {
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
            [self drawData];
        }];
    } else if (connection.tag == PROCESS_GET_PICURL) {
        RYKWmpImg *item = [[RYKWmpImg alloc] init];
        [item parseData:data complete:^(RYKWmpImg *item) {
            [self.indicator hide];
            if (!item.picUrl) {
                self.img.image = [UIImage imageNamed:self.currItem.key];
                self.flag = @"0";
            } else {
                self.img.url = [RYKWebService itemImageFullUrl:[NSString stringWithFormat:@"uniService/Resource/custMobile/%@/%@",[RYKWebService sharedUser].userId,item.picUrl]];
               [self.img loadImage];
                self.flag = @"1";
           }
        }];
    } else if (connection.tag == PROCESS_POST_DEFAU) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *msg) {
            if ([msg.code isEqualToString:@"1"]) {
                self.indicator.text = msg.msg;
                [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0f];
                self.img.image = [UIImage imageNamed:self.currItem.key];
            } else {
                self.indicator.text = msg.msg;
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
            }
        }];
    } else if (connection.tag == PROCESS_POST_MODIFY) {
        ConsSystemMsg *msg = [[ConsSystemMsg alloc] init];
        [msg parseData:data complete:^(ConsSystemMsg *msg) {
            if ([msg.code isEqualToString:@"1"]) {
                self.indicator.text = msg.msg;
                self.img.url = [RYKWebService itemImageFullUrl:self.uploadImgName];
                [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0f];
            } else {
                self.indicator.text = msg.msg;
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
            }
        }];
    }
}
#pragma mark ASIHttpRequest Delegate
- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"%@", [request error]);
    self.indicator.text = @"上传失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)request:(ASIHTTPRequest *)request didReceiveData:(NSData *)data
{
    ConsSystemMsg * sysMsg = [[ConsSystemMsg alloc] init];
    [sysMsg parseData:data];
    if ([sysMsg.code isEqualToString:@"1"]) {
        //插入图片信息
        [self insertPicData];
    } else {
        self.indicator.text = sysMsg.msg;
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    }
    return;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
