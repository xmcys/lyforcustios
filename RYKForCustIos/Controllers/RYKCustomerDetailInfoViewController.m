//
//  RYKCustomerDetailInfoViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "RYKCustomerDetailInfoViewController.h"
#import "CTTableView.h"
#import "RYKRecordsofonsViewController.h"
#import "CTAlertView.h"


@interface RYKCustomerDetailInfoViewController ()<UITableViewDelegate,UITableViewDataSource,CTTableViewDelegate>
@property (strong, nonatomic) CTTableView *tableview;
@property (strong, nonatomic) NSMutableArray *custinfoArray;

@end

@implementation RYKCustomerDetailInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.custinfoArray = [[NSMutableArray alloc] init];
    UIButton *btnsearch = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnsearch setBackgroundImage:[UIImage imageNamed:@"search2"] forState:UIControlStateNormal];
    btnsearch.frame = CGRectMake(0, 0, 21, 17);
    [btnsearch addTarget:self action:@selector(btnsearch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnright = [[UIBarButtonItem alloc] initWithCustomView:btnsearch];
    self.navigationItem.rightBarButtonItem = btnright;
}

- (void)btnsearch:(UIButton *)btn
{

    RYKRecordsofonsViewController *controller = [[RYKRecordsofonsViewController alloc] initWithuserId:self.msg.userId userName:self.msg.userName mobilePhone:self.msg.mobilphone];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"顾客资料";
    if (self.tableview == nil) {
        self.tableview = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tableview.backgroundColor = [UIColor clearColor];
        self.tableview.delegate = self;
        self.tableview.pullUpViewEnabled = NO;
        self.tableview.pullDownViewEnabled = NO;
        self.tableview.dataSource = self;
        self.tableview.ctTableViewDelegate = self;
        [self.view addSubview:self.tableview];
    }
}

#pragma mark - CTTableview Delegate DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 3;
    }
    else if (section == 1){
        return 2;
    }
    else{
        if ([self.msg.userAddrSec isEqualToString:@""]) {
            return 2;
        }
        return 3;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    else{
        return 20;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    cell.backgroundColor = [UIColor whiteColor];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //分割线
    UIView *topview = [[UIView alloc] initWithFrame:CGRectMake(0, 49, tableView.frame.size.width, 1)];
    topview.layer.borderWidth = 1;
    topview.layer.borderColor = [[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:0.5] CGColor];
    [cell addSubview:topview];
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(20, 17, 17, 17)];
    img.contentMode = UIViewContentModeScaleToFill;
    [cell addSubview:img];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, tableView.frame.size.width - 200, 50)];
    title.backgroundColor = [UIColor clearColor];
    title.font = [UIFont systemFontOfSize:15.0];
    title.textColor = [UIColor colorWithHue:43.0/255.0 saturation:43.0/255.0 brightness:43.0/255.0 alpha:1];
    [cell addSubview:title];
    
    UILabel *detail = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, self.view.bounds.size.width - 170, 50)];
    detail.font = [UIFont systemFontOfSize:15.0];
    detail.textAlignment = NSTextAlignmentRight;
    detail.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
    [cell addSubview:detail];
    
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        img.image = [UIImage imageNamed:@"icon_05"];
        title.text = @"姓名";
        detail.text = self.msg.userName;
    }
    else if (indexPath.section == 0 && indexPath.row == 1) {
        img.image = [UIImage imageNamed:@"icon_08"];
        title.text = @"性别";
        if ([self.msg.sex isEqualToString:@"1"]) {
            detail.text = @"男";
        }else{
            detail.text = @"女";
        }
    }
    else if (indexPath.section == 0 && indexPath.row == 2) {
        img.image = [UIImage imageNamed:@"icon_09"];
        title.text = @"出生日期";
        if (self.msg.birthday.length >= 10) {
            NSString *str = [self.msg.birthday substringToIndex:11];
            detail.text = str;
        }
    }
    else if (indexPath.section == 1 && indexPath.row == 0) {
        img.image = [UIImage imageNamed:@"icon_10"];
        title.text = @"电子邮箱";
        detail.text = self.msg.email;
        if ([self.msg.email isEqualToString:@"null"]) {
            detail.text = @"";
        }
    }
    else if (indexPath.section == 1 && indexPath.row == 1) {
        img.image = [UIImage imageNamed:@"icon_11"];
        title.text = @"联系电话";
        detail.text = self.msg.mobilphone;
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        img.image = [UIImage imageNamed:@"icon_12"];
        title.text = @"收货地址";
        detail.text = @"";
    }
    else if (indexPath.section == 2 && indexPath.row == 1) {
        UILabel *row = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.view.frame.size.width, 50)];
        row.font = [UIFont systemFontOfSize:15.0];
        row.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
        row.lineBreakMode = NSLineBreakByCharWrapping;
        row.numberOfLines = 0;
        [cell addSubview:row];
        row.text = self.msg.userAddr;
    }else {
        UILabel *row = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.view.frame.size.width, 50)];
        row.font = [UIFont systemFontOfSize:15.0];
        row.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
        row.lineBreakMode = NSLineBreakByCharWrapping;
        row.numberOfLines = 0;
        [cell addSubview:row];
        row.text = self.msg.userAddrSec;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1 && indexPath.row == 1) {
        CTAlertView *alertview = [[CTAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"是否呼叫%@", self.msg.mobilphone] delegate:self cancelButtonTitle:@"取消" confirmButtonTitle:@"确定"];
        [alertview show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CTAlertview Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    if (index == 0) {
        return;
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", self.msg.mobilphone]]];
}

@end
