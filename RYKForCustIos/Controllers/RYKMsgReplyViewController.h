//
//  RYKMsgReplyViewController.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-25.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

@protocol RefreshMsgListDelegate <NSObject>

@optional

- (void)RefreshGetMoreMsg:(BOOL)Refresh;


@end


#import "CTViewController.h"
#import "RYKMsg.h"

@interface RYKMsgReplyViewController : CTViewController

@property (strong, nonatomic) NSString *opinionSeq;
@property (strong, nonatomic) RYKMsg *msg;
@property (weak, nonatomic) id<RefreshMsgListDelegate>delegate;



@end
