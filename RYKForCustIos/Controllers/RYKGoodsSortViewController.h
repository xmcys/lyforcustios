//
//  RYKGoodsSortViewController.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-11-19.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@class RYKGoodsSortCell;
@class RYKGoodsSortInfo;

@protocol GoodsSortCellDelegate <NSObject>

- (void)goodsSortCellBtnMoveUpOnClick:(RYKGoodsSortCell *)cell;
- (void)goodsSortCellBtnMoveDownOnClick:(RYKGoodsSortCell *)cell;
- (void)goodsSortCellBtnMoveToTopOnClick:(RYKGoodsSortCell *)cell;

@end

@interface RYKGoodsSortCell : UITableViewCell

@property (nonatomic, weak) id<GoodsSortCellDelegate> delegate;
@property (nonatomic, strong) RYKGoodsSortInfo *goodSortInfo;

@end

@interface RYKGoodsSortViewController : CTViewController

@end
