//
//  CustAboutController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-10.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKAboutController.H"
#import <QuartzCore/QuartzCore.h>

@interface RYKAboutController ()

@property (nonatomic, strong) IBOutlet UIView * contentView;
@property (nonatomic, strong) IBOutlet UIScrollView * sv;
@property (nonatomic, strong) IBOutlet UILabel * appName;
@property (nonatomic, strong) IBOutlet UILabel * appVersion;

- (void)back;

@end

@implementation RYKAboutController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"关于";
        self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.contentView.layer.borderWidth = 0.5;
    self.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.contentView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.contentView.layer.shadowOpacity = 0.1;
    self.contentView.layer.shadowRadius = 1;
    
    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    self.appVersion.text = [NSString stringWithFormat:@"iPhone V%@", [infoDict objectForKey:@"CFBundleShortVersionString"]];
    
    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width / 3, 44)];
    leftView.backgroundColor = [UIColor clearColor];
    leftView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    [leftView addGestureRecognizer:tapGes];
    
    UIImageView * leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBack"]];
    [leftImg sizeToFit];
    leftImg.center = CGPointMake(leftImg.frame.size.width * 0.75, leftView.frame.size.height / 2);
    [leftView addSubview:leftImg];
    
    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftView];
    self.navigationItem.leftBarButtonItem = leftItem;
    [self.view addSubview:self.sv];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.sv.frame = self.view.bounds;
    self.sv.contentSize = CGSizeMake(self.sv.frame.size.width, 520);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
