//
//  ConsPwdModifyController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-28.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "RYKPwdModifyController.h"
#import "RYKWebService.h"
#import "ConsSystemMsg.h"

@interface RYKPwdModifyController () <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIView * container;
@property (nonatomic, strong) IBOutlet UITextField * pwdField;
@property (nonatomic, strong) IBOutlet UITextField * pwdNewField;
@property (nonatomic, strong) IBOutlet UITextField * pwdConfirmField;

- (void)submit;

@end

@implementation RYKPwdModifyController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"密码修改";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"提交按钮"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"提交按钮"] forState:UIControlStateHighlighted];
    rightBtn.showsTouchWhenHighlighted = YES;
    [rightBtn addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
    
	[self.view addSubview:self.container];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)submit
{
    [self.pwdField resignFirstResponder];
    [self.pwdNewField resignFirstResponder];
    [self.pwdConfirmField resignFirstResponder];
    
    if (self.indicator.showing) {
        return;
    }
    
    NSString * pwd = self.pwdField.text;
    NSString * pwdNew = self.pwdNewField.text;
    NSString * pwdConfirm = self.pwdConfirmField.text;
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSString * savedPsw = (NSString *)[defaults valueForKey:@"PASSWORD"];
    
    if (pwd.length == 0) {
        self.indicator.text = @"请输入当前密码";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    if (![pwd isEqualToString:savedPsw]) {
        self.indicator.text = @"当前密码有误";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    if (pwdNew.length < 4 || pwdNew.length > 16){
        self.indicator.text = @"请输入4-16位新密码";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    if (![pwdConfirm isEqualToString:pwdNew]) {
        self.indicator.text = @"两次密码不一致";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    self.indicator.text = @"正在提交..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];
    
    NSString * params = [NSString stringWithFormat:@"<Test><userId>%@</userId><oldPwd>%@</oldPwd><newPwd>%@</newPwd></Test>", [RYKWebService sharedUser].userId, pwd, pwdNew];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[RYKWebService savePasswordUrl] params:params delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.pwdField) {
        [self.pwdNewField becomeFirstResponder];
    } else if (textField == self.pwdNewField){
        [self.pwdConfirmField becomeFirstResponder];
    } else {
        [self.pwdConfirmField resignFirstResponder];
    }
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"提交失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    ConsSystemMsg * msg = [[ConsSystemMsg alloc] init];
    [msg parseData:data];
    if ([msg.code isEqualToString:@"1"]) {
        
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:self.pwdNewField.text forKey:@"PASSWORD"];
        
        self.indicator.text = @"修改成功";
        [self.indicator hideWithSate:CTIndicateStateDone afterDelay:1.2];
        
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([msg.code isEqualToString:@"5"]) {
        // 旧密码错误
        self.indicator.text = msg.msg;
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.2];
    } else {
        // 其他修改失败
        self.indicator.text = @"修改失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.2];
    }
}

@end
