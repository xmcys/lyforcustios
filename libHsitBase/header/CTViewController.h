//
//  CTViewController.h
//  ChyoTools
//
//  Created by 陈 宏超 on 13-12-31.
//  Copyright (c) 2013年 Chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CTIndicateView.h"
#import "CTURLConnection.h"

@interface ExtraNavView : UIView

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIView * leftView;
@property (nonatomic, strong) UIView * rightView;


- (void)removeAllSubViewsInView:(UIView *)view;

@end

@interface CTViewController : UIViewController <CTURLConnectionDelegate>


@property (nonatomic, strong) CTIndicateView * indicator;

@property (nonatomic) float bottomOffset;
// 设置状态栏是否隐藏
@property (nonatomic) BOOL stautsBarHidden;
// 额外的导航栏（默认hidden）
@property (nonatomic, strong) ExtraNavView * extraNavView;
// 设置是否显示额外的导航栏（YES时，自带导航栏将被隐藏）
- (void)setExtraNavViewHidden:(BOOL)hidden;
// 设置Tab栏元素
- (void)setTabBarItemWithTitle:(NSString *)title titleColor:(UIColor *)color titleSelectedColor:(UIColor *)selectedColor image:(UIImage *)image selectedImage:(UIImage *)selectedImage;
@end
