//
//  CMURLConnection.h
//  CM_APP
//
//  Created by 陈 宏超 on 13-4-1.
//  Copyright (c) 2013年 Xiamen Icss-Haisheng Information Technology Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CTURLConnection;

@protocol CTURLConnectionDelegate <NSObject>

@optional

// 请求结束执行的方法
- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data;
// 请求失败时执行的方法
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error;

@end

@interface CTURLConnection : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, assign) id<CTURLConnectionDelegate> delegate;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * params;
@property (nonatomic) int tag;
@property (nonatomic) id userInfo;

// 标识是否需要输出log信息
+ (void)setNeedsLog:(BOOL)needsLog;

// 使用 GET 方式来请求数据
- (id)initWithGetMethodUrl:(NSString *)url delegate:(id<CTURLConnectionDelegate>)delegate;
// 使用 PUT 方式来请求或提交数据
- (id)initWithPutMethodUrl:(NSString *)url params:(NSString *)params delegate:(id<CTURLConnectionDelegate>)delegate;
- (void)start;
- (void)cancel;

@end
